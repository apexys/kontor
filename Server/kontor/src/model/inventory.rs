///Basic inventory definition
#[derive(SqlMacro, Serialize)]
pub struct Inventory{
    pub id: i64,
    pub name: String, //Name of the inventory
    pub description: String //Description of the inventory
}

impl Inventory{
    pub fn new(name: String, description: String) -> Self{
        Inventory{
            id: -1,
            name,
            description
        }
    }
}

///Linking table that describes what products are stored in which inventories
#[derive(SqlMacro, Serialize, Clone)]
pub struct InventoryDefinition{
    pub id: i64,
    pub inventory_id: i64, //Id of the inventory
    pub product_id: i64, //Id of the product that is stored
    pub amount: i64 //Minimum amount to store
}

impl InventoryDefinition{
    pub fn new(inventory_id: i64, product_id: i64, amount: i64) -> Self{
        InventoryDefinition{
            id: -1,
            inventory_id,
            product_id,
            amount
        }
    }
}
