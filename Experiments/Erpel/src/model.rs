//This is the model, the part of the application that represents the user data

//First off, we need a few imports
extern crate rusqlite;

use self::rusqlite::Connection;

//Let's define a trait that all objects to be stored in the database will implement, so we have a few common methods
pub trait DbObject {
    fn initialize(conn: &Connection); //An initializer that will create a table for the object in the database
    fn save(&self, conn: &Connection); //An update function that will persist an object in the database 
}
