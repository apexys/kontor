def urldecode(str):
    while "%0" in str or "%1" in str or "%2" in str or "%3" in str or "%4" in str or "%5" in str or "%6" in str or "%7" in str or "%8" in str or "%9" in str or "%A" in str or "%B" in str or "%C" in str or "%D" in str or "%E" in str or "%F" in str:
        code = str[str.index("%"):][:3]
        char = chr(int(code[1:],16))
        str = str.replace(code, char)
    return str

def urlencode(s):
    s = str(s)
    result = ""
    for c in s:
        if not c in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~:/?#[]@!$&'()*+,;=":
            result += "%" + ('0' + hex(ord(c))[2:])[-2:].upper()
        else:
            result += c
    return result

def decode_formdata(formdatastr):
    tuples = formdatastr.split("&")
    env = {}
    for t in tuples:
        if("=" in t):
            t = t.split("=")
            t[0] = urldecode(t[0])
            t[1] = urldecode(t[1])
            env[t[0]] = t[1]
    return env

def encode_formdata(env):
    counter = 0
    result = ""
    for key in env:
        value = env[key]
        tuple = urlencode(key) + "=" + urlencode(value)
        if(counter > 0):
            result += "&"
        result += tuple
        counter += 1
    return result
