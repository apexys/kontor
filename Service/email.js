var nodemailer = require('nodemailer');

module.exports.sendMail = function sendMail(account, password, receiver, subject, mailtext){
    return new Promise((res, rej) => {
        var [username, host_and_port] = account.split("@");
        var host = "";
        var port = 587;
        if(host_and_port.includes(":")){
            [host, port] = host_and_port.split(":");
            port = parseInt(port);
        }else{
            host = host_and_port;
        }
    
        var transport = nodemailer.createTransport({
            host: host,
            port: port,
            auth:{
                user: username,
                pass: password
            }
        });
    
        let mailOptions = {
            from: account,
            to: receiver,
            subject: subject,
            text: mailtext
        };
    
        transport.sendMail(mailOptions, function(error, info){
            if(error){
                rej(error)
            }else{
                res(info)
            }
        });
    });  
}