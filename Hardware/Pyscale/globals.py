from logger import logger

#0. Settings
from settings import Settings
settings = Settings()

# 1. Asyncio Event loop (needed by pretty much everything in here)
import uasyncio as asyncio

# 2. Scale infrastructure
from scale import Scale
scale = Scale(settings.get("calibration_factor"), settings.get("scale_offset"))

# 3. Status
from status import Status
status = Status()

# 3. Variable the scale can write into asynchronously
scalereading = 0

# 4. Wifi
import network
ap = network.WLAN(network.AP_IF)
sta = network.WLAN(network.STA_IF)
wifi_settings = {
    "AP_SSID": "",
    "AP_PSK": ""
}

async def sta_connected():
    while not sta.isconnected() or sta.ifconfig()[0] == "0.0.0.0":
        await asyncio.sleep_ms(200)

def is_sta_connected():
    return sta.isconnected() and sta.ifconfig()[0] != "0.0.0.0"

# 5. Write the wifi connection data to a file
def writeWifiData(AP_SSID, AP_PSK, STA_SSID, STA_PSK):
    settings.set("AP_SSID", AP_SSID)
    settings.set("AP_PSK", AP_PSK)
    settings.set("STA_SSID", STA_SSID)
    settings.set("STA_PSK", STA_PSK)
    settings.save()

# 6. Read that file and configure the interfaces
def configureWifi():
    if settings.get("AP_SSID") == None or len(str(settings.get("AP_SSID"))) < 1:
        settings.set("AP_SSID", "pyscale")
        settings.save()
    if settings.get("AP_PSK") == None or len(str(settings.get("AP_SSID"))) < 1:
        settings.set("AP_PSK", "pyscalepyscale")      
        settings.save()

    #Configure AP

    if(wifi_settings["AP_SSID"] != settings.get("AP_SSID") or wifi_settings["AP_PSK"] != settings.get("AP_PSK")):
        ap.active(True)
        ap.config(essid=settings.get("AP_SSID"), password=settings.get("AP_PSK"))
        logger().log("AP config:" + str(ap.ifconfig()), "PHY")
        wifi_settings["AP_SSID"] = settings.get("AP_SSID")
        wifi_settings["AP_PSK"] = settings.get("AP_PSK")

    #Only configure the STA if there's something saved for it
    if settings.get("STA_SSID") != None and len(settings.get("STA_SSID")) > 0:
        sta.active(True)
        sta.connect(settings.get("STA_SSID"), settings.get("STA_PSK"))
        async def printSTAInfo():
            while not sta.isconnected() or sta.ifconfig()[0] == "0.0.0.0":
                await asyncio.sleep_ms(100)
            logger().log("STA config: " + str(sta.ifconfig()), "PHY") 
        asyncio.get_event_loop().create_task(printSTAInfo())

    #Turn the config wifi off after 15 Minutes
    async def turnWifiOff():
        logger().log("Turning wifi off after 15 Minutes", "PHY")
        ap.active(False)
    asyncio.get_event_loop().call_later(15 * 60, turnWifiOff())
