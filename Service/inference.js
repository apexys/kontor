//From ([{products: [{product_id, amount}], timestamp}], {product_id-> minimum}) to {product_id -> timestamp2} where timestamp2 is the time where there are minimum items of product_id
function inferTicksEmpty(inventory_states, minima){ //minima: Map(product_id -> minimum)
    var product_ids = inventory_states.reduce((prev, next) => prev.concat(next.products.filter(p => ! prev.includes(p))), []); //Gather all product ids
    //For each
    return product_ids.reduce((prev, next) => Object.defineProperty(prev, next,  lin_reg_zero(inventory_states.map(invstate => [timestamp, (invstate.products.find(p => p.product_id == next) || {amount: 0}).amount]), minima[next] || 0)),{}) 
}

function lin_reg_zero(points, minimum){ //Points: [[x,y],...]
    let mean_x = points.reduce((prev, next) => prev += next[0], 0) / points.length;
    let mean_y = points.reduce((prev, next) => prev += next[1], 0) / points.length;
    let a1 = points.reduce((prev, next) => prev += ((next[0] - mean_x) * (next[1] - mean_y)), 0) / points.reduce((prev, next) => prev += Math.pow(next[0] - mean_x,2));
    let a0 = mean_y - (a1 * mean_x);
    //f(x) = a1*x + a0
    return (minimum - a0) / a1;
}