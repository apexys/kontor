import uasyncio as asyncio
import globals

DEBUG = False

async def request(url, headers = {}, data = None, method="GET", headers_only=False, binary_body=False):
    #0. Wait until STA is connected
    await globals.sta_connected()
    
    #1. Split URL
    urlparts = url.split("/", 3)
    protocol = urlparts[0]
    host = urlparts[2]

    #2. Set the correct port: 80 if http, 443 if https, custom if given
    port = 80
    if protocol == "https:":
        port = 443

    if ":" in host:
        hostparts = host.split(":")
        host = hostparts[0]
        port = int(hostparts[1])

    #3. Find the requested path
    if len(urlparts) == 4:
        path = "/" + urlparts[3]
    else:
        path = "/"
        
    if not "Host" in headers and protocol == "http":
        headers["Host"] = host

    if not data == None and not "Content-Length" in headers:
        headers["Content-Length"] = str(len(data))
    else:
        if data != None and headers["Content-Length"] != str(len(data)): 
            if DEBUG: print("Content-Length already in headers and WRONG: " + headers["Content-Length"] + " (data length is " + str(len(data)) + ", correcting this)" )
            headers["Content-Length"] = str(len(data))

    
    if DEBUG: print("Connecting to " + str(host) + " on port " + str(port) + " using protocol " + protocol[:-1])
    #4. Open the connection
    reader, writer = yield from asyncio.open_connection(host, port, protocol == "https:")
    if DEBUG: print("Connection established")

    #5.a Assemble Query
    query = method + " " + path + " " + "HTTP/1.1\r\n"
    query += "Host: " + host+ "\r\n"
    for key in headers:
        query += (key + ": " + headers[key] + "\r\n")
    
    query += "\r\n"
    if not data == None:
        query += data
    
    if DEBUG: 
        print("Query: ")
        print(query)

    #5.b Write Query
    await writer.awrite(query)
    if DEBUG: print("Request written for " + method + " request to " + path)
    response = {}

    #6. Parse Status
    statusline = yield from reader.readline()
    statusline = statusline.decode("utf-8")
    statusline = statusline.rstrip()
    statusparts = statusline.split(" ")
    response["version"] = statusparts[0]
    response["status"] = statusparts[1]
    if len(statusline) > 2:
        response["status_extra"] = statusparts[2]

    if DEBUG: print("Status parsed")

    #7. Parse headers and cookies
    headers = {}
    cookies = {}
    encoding = "utf-8"
    while True:
        headers_line = yield from reader.readline()
        headers_line = headers_line.decode(encoding)
        headers_line = headers_line.rstrip()
        if len(headers_line) < 3:
            break
        headers_parts = headers_line.split(": ")
        #Special treatment for cookies
        if headers_parts[0] == "Set-Cookie":
            c = headers_parts[1]
            if ";" in c:
                c = c[:c.index(";")]
            c_name, c_value = c.split("=")
            cookies[c_name] = c_value
        #Special treatment for content-type
        if headers_parts[0] == "Content-Type" and "charset" in headers_parts[1]:
            charset = headers_parts[1]
            charset = charset[charset.index("charset="):]
            if ";" in charset:
                charset = charset[:charset.index(";")]
            encoding = charset.lower()

        headers[headers_parts[0]] = headers_parts[1]
    
    response["headers"] = headers
    response["cookies"] = cookies

    if DEBUG: print("Headers parsed")

    #8. Read content
    if not headers_only:
        if "Content-Length" in headers:
            content_length = int(headers["Content-Length"])
            response["body"] = yield from reader.read(content_length)
            if not binary_body: response["body"] = response["body"].decode(encoding)
        else:
            while True:
                line  = yield from reader.readline()
                if not binary_body: line = line.decode(encoding)
                if not line: 
                    break
                else:
                    response["body"] +=  line
        if DEBUG: print("Body read")
    await reader.aclose()
    if DEBUG: print("Stream closed")
    return response