
use std::fs::File;
use std::io::prelude::*;
use std::error::Error;

#[derive(Serialize, Deserialize)]
pub enum Usertype{
    User,
    Admin,
    API
}

#[derive(Serialize, Deserialize)]
pub struct User{
    id: i16,
    usertype: Usertype,
    name: String,
    hash: String
}

extern crate bcrypt;

impl User {
    pub fn new(id: i16, usertype: Usertype, name: &str, password: &str)-> User{
        User{
            id: id,
            usertype: usertype,
            name: String::from(name),
            hash: bcrypt::hash(password, bcrypt::DEFAULT_COST).unwrap()
        }
    }

    pub fn verify(&self, password: &str)-> bool{
        bcrypt::verify(password, &self.hash).unwrap()
    }
}

use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct Logins{
    users: Vec<User>,
    tokens: HashMap<String,i16>,
    next_id: i16
}

extern crate serde_json;
use uuid::Uuid;

impl Logins {
    fn empty() -> Logins
    {
        Logins{
            users: Vec::new(),
            tokens: HashMap::new(),
            next_id: 0
        }
    }

    fn deserialize() -> Result<Logins, Box<Error>>{
        let mut buffer = String::new();
        let mut f = File::open("users.json")?;
        f.read_to_string(&mut buffer)?;
        Ok(serde_json::from_str(&buffer)?)
    }

    pub fn load()->Logins{
        match Logins::deserialize(){
            Ok(logins) => logins,
            Err(_) => Logins::empty()
        }
    }

    fn serialize(&self) -> Result<Vec<u8>, Box<Error>>{
        Ok(serde_json::to_vec_pretty(&self)?)
    }

    pub fn save(&self){
        match Logins::serialize(&self){
            Ok(s) => {
                let mut f = File::open("users.json").unwrap();
                f.write_all(&s).unwrap_or_default();
                f.sync_all().unwrap_or_default();
            },
            Err(e) => println!("Error saving users: {}", e)
        }
    }

    pub fn is_logged_in(&self, token: &str) -> bool {
        self.tokens.contains_key(token)
    }

    pub fn log_in(&mut self, username: &str, password: &str)-> Option<String>{
        match self.users.iter().find(|u| u.name == username){
            None => None,
            Some(u) => {
                if u.verify(password){
                    let token = Uuid::new_v4().hyphenated().to_string();
                    self.tokens.insert(String::from(token.as_str()), u.id);
                    Some(token)
                }else{
                    None
                }
            }
        } 
    }

    pub fn log_out(&mut self, token: &str)->bool{
        self.tokens.remove(token).is_some()
    }

    pub fn user_exists(&self, username: &str) -> bool{
        self.users.iter().any(|u| u.name == username)
    }

    pub fn create_user(&mut self, usertype: Usertype, username: &str, password: &str)-> Result<String, &str>{
        if self.user_exists(username){
            Err("Username already taken")
        }else{
            let u = User::new(self.next_id, usertype,username, password);
            self.next_id = self.next_id + 1;
            self.users.push(u);
            self.save();
            Ok(self.log_in(username, password).unwrap())//No way this can fail ;)    
        }
    }


}
