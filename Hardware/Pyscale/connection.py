import request
import helpers
from globals import settings, is_sta_connected

class Connection:
    def __init__(self):
        self.token = None
        self.logged_in = False

    async def check_connection(self):
        try:
            if not is_sta_connected(): 
                return False
            if (not settings.has("Server-URL")) or (not settings.has("Server-Username")) or (not settings.has("Server-Password")): 
                return False
            resp = yield from request.request(settings.get("Server-URL"))
            return resp["status"] == "200" and resp["headers"]["Server"] == "Rocket"
        except:
           return False

    async def login(self):
        logindata = {
            "username": settings.get("Server-Username"),
            "password": settings.get("Server-Password")
        }
        resp = yield from request.request(settings.get("Server-URL") + "/login", headers=self.getFormApplicationTypeHeaders(), data=helpers.encode_formdata(logindata),method="POST", headers_only=True)
        if resp["status"] == "303" and "token" in resp["cookies"]:
            self.token = resp["cookies"]["token"]
            self.logged_in = True
        else:
            self.logged_in = False
        return self.logged_in

    def getCookieHeaders(self, headers = {}):
        headers["Cookie"] = "token=" + self.token
        return headers

    def getFormApplicationTypeHeaders(self, headers = {}):
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        return headers

    async def logout(self):
        resp = yield from request.request(settings.get("Server-URL") + "/logout", headers=self.getCookieHeaders(), headers_only=True)
        if resp["status"] == "303":
            self.token = None
            self.logged_in = False
        return not self.logged_in

    async def get_inventories(self, string_instead_of_json=False):
        if not self.logged_in:
            await self.login()
        resp = yield from request.request(settings.get("Server-URL") + "/inventories_json", headers=self.getCookieHeaders(), headers_only=False)
        if string_instead_of_json:
            return resp["body"]
        else:
            import ujson
            return ujson.loads(resp["body"])

    async def get_server_report(self):
        if not self.logged_in:
            await self.login()
        resp = yield from request.request(settings.get("Server-URL") + "/sensors/latest/" + str(settings.get("Inventory-Id")), headers=self.getCookieHeaders(), headers_only=False)
        import ujson
        return ujson.loads(resp["body"])

    async def report_weight(self, weight):
        if not self.logged_in:
            await self.login()
        report_data = {
            "weight_grams": int(weight),
            "inventory_id": settings.get("Inventory-Id")
        }
        resp = yield from request.request(settings.get("Server-URL") + "/sensors/report", headers=self.getCookieHeaders(self.getFormApplicationTypeHeaders()), data=helpers.encode_formdata(report_data),method="POST", headers_only=True)
        if resp["status"] == "303":
            return True
        else:
            return False