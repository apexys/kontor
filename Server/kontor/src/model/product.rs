#[derive(SqlMacro, Serialize, Clone, Debug)]
pub struct Product{
    pub id: i64,
    pub name: String,
    pub supplier: String,
    pub weight_grams: i64
}

impl Product{
    pub fn new(name: String, supplier: String, weight_grams: i64) -> Self{
        Product{
            id: -1,
            name,
            supplier,
            weight_grams
        }
    }
}