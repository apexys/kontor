#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

#[macro_use]
extern crate serde_derive;

extern crate serde;
extern crate serde_json;

extern crate uuid;

mod model;
mod user;

use rocket::response::NamedFile;
use std::path::Path;
use std::path::PathBuf;

#[get("/")]
fn index() -> Option<NamedFile> {
    NamedFile::open(Path::new("../templates/login.html")).ok()
}

#[get("/static/<file..>")]
fn files(file: PathBuf) -> Result<NamedFile, String>  {
    let path = Path::new("../templates/static/").join(file);
    NamedFile::open(&path).map_err(|_| format!("Bad path: /static/{:?}", path))
}

fn main() {
    rocket::ignite().mount("/", routes![index, files]).launch();
}
