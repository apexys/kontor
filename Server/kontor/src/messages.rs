
pub const USERNAME_CHANGED: &'static str = "update:Username changed";
pub const USERNAME_TAKEN: &'static str = "error:Username already taken";
pub const PASSWORD_CHANGED: &'static str = "update:Password changed";
pub const PASSWORD_TOO_SHORT: &'static str = "error: Password too short";
pub const USER_CREATED: &'static str = "update:User created";
pub const PRODUCT_CREATED: &'static str = "update:Product created";
pub const PRODUCT_ALREADY_EXISTS: &'static str = "error:A product with this name already exists";
pub const PRODUCT_WEIGHT_WRONG: &'static str = "error:The product weight is defined incorrectly";
pub const PRODUCT_UPDATED: &'static str = "update:Product definition changed";
pub const PRODUCT_DELETED: &'static str = "update:Product deleted";
pub const INVENTORY_CREATED: &'static str = "update:Inventory created";
pub const INVENTORY_UPDATED: &'static str = "update:Inventory updated";
pub const INVENTORY_DELETED: &'static str = "update:Inventory deleted";
pub const INVENTORY_EXISTS: &'static str = "error:An inventory with this name already exists";
pub const REPORT_CREATED: &'static str = "update:Report created";
pub const SENSOR_REPORT_CREATED: &'static str = "update: Sensor report created";