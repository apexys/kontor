from logger import logger
from hx711 import HX711
import machine
import uasyncio as asyncio
from globals import settings


class Scale:
    def start_reading(self):
        async def get_async_reading():
            reading_index = 0
            first_tare = False
            while True:
                await asyncio.sleep_ms(10)
                self.readings[reading_index] = self.__get_reading()
                reading_index += 1
                if reading_index >= self.maxreadings:
                    reading_index = 0
                    if not first_tare:
                        first_tare = True
                        self.tared = True
                        #Don't tare, we will need it to just work after a soft reset
        asyncio.get_event_loop().call_soon(get_async_reading())

    def get_reading(self, factor = None):
        if(len(self.readings) == 0):
            logger().log("no readings!", "SCALE")
            return self.__get_reading()
        else:
            avg = 0
            for value in self.readings:
                avg += value
            if factor == None:
                return int((avg / len(self.readings)) * self.factor)
            else:
                return int((avg / len(self.readings)) * factor)

    def set_factor(self, factor):
        self.factor = factor

    def __init__(self, factor = 1, initial_offset = 0):
        machine.freq(240000000)
        self.driver = HX711(d_out=4, pd_sck=5, channel=HX711.CHANNEL_A_128)
        if initial_offset == None:
            initial_offset = 0
            logger().log("Set offset to 0", "SCALE")
        self.offset = initial_offset
        logger().log("initial_offset = " + str(initial_offset), "SCALE")
        self.maxreadings = 12
        self.readings = [0] * self.maxreadings
        self.tared = False
        logger().log("factor = " + str(factor), "SCALE")
        if factor == None:
            factor = 1
            logger().log("Set factor to 1", "SCALE")
        self.factor = factor
        asyncio.get_event_loop().create_task(self.start_reading)

    def __get_reading(self):
        reading = self.driver.read()
        while reading == -1:
            reading = self.driver.read()
        return reading - self.offset

    def tare(self):
        reading = self.get_reading(1)
        self.offset += reading
        logger().log("Set offset to " + str(self.offset), "SCALE")
        settings.set("scale_offset", self.offset)
        self.readings = [0] * self.maxreadings
        self.tared = True