from machine import Pin
import uasyncio as asyncio

class Status:
    def start_update(self):
        async def on():
            self.ledpin.value(1)
        
        async def off():
            self.ledpin.value(0)

        async def slow_blink():
            while True:
                self.ledpin.value(1)
                await asyncio.sleep(500)
                self.ledpin.value(0)
                await asyncio.sleep(500)
        
        async def fast_blink():
            while True:
                self.ledpin.value(1)
                await asyncio.sleep(100)
                self.ledpin.value(0)
                await asyncio.sleep(100)


        async def statusupdate():
            last_status = -1
            while True:
                if self.status != last_status:
                    last_status = self.status
                    if self.status == self.Code["ok"]:
                        asyncio.get_event_loop().call_soon(on())
                    elif self.status == self.Code["waiting"]:
                        asyncio.get_event_loop().call_soon(slow_blink())
                    elif self.status == self.Code["transmitting"]:
                        asyncio.get_event_loop().call_soon(fast_blink())
                    else:
                        asyncio.get_event_loop().call_soon(off())
                await asyncio.sleep_ms(100)

        asyncio.get_event_loop().call_soon(statusupdate())

    def set(self, status):
        self.status = status
    
    def __init__(self):
        self.Code = {
            "ok": 0,
            "waiting": 1,
            "transmitting": 2,
            "error": 3
        }
        self.status = 0
        self.ledpin = Pin(2, Pin.OUT)
        asyncio.get_event_loop().create_task(self.start_update)
