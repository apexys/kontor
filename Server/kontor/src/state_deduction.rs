use model::Persistance;
use std::collections::HashMap;

use model::inventory::{InventoryDefinition};
use model::report::{InventoryReport, ProductReport};
use model::product::{Product};
use sqlite_traits::dbobject::DbObject;
use std::error::Error;
use std::collections::BTreeSet;
use chrono::prelude::*;

#[derive(Serialize, Clone)]
pub struct ProductState{
    pub product_name: String,
    pub product_id: i64,
    pub amount_to_store: i64,
    pub amount_stored: i64,
    pub maximum: i64
}

#[derive(Serialize)]
pub struct InventoryState{
    pub inventory_id: i64,
    pub timestamp: i64,
    pub product_states: Vec<ProductState>
}


pub fn get_latest_inventory_states(persistance: &Persistance, inventory_id: i64, number: i64) -> Result<Vec<InventoryState>, Box<Error>>{

    //1. Preparation: Get all inventory definitions and products. There probably aren't that many and it saves us A LOT OF QUERIES. We could do it though :D
    let inventory_definitions = InventoryDefinition::query(persistance.get_conn()?).all()?;
    let products = Product::query(persistance.get_conn()?).all()?;

    //2. Let's prepare a closure for creating a HashMap of InventoryReports, since doing that inline would clutter everything even more
    let product_reports_for_inventory_report = |inventory_report_id: i64| -> HashMap<i64, ProductReport> {
        persistance.get_conn()
            .and_then(|conn| 
                ProductReport::query(conn)
                    .Where(ProductReport::fields().report_id, inventory_report_id)
                    .all()
                    .map(|reports| reports.into_iter().fold(HashMap::new(), |mut map, pr| {map.insert(pr.product_id, pr); map}))
            )
            .unwrap_or(HashMap::new())
    };

    //3. Same for getting a filtered iterator over the inventory definitions
    let inventory_definitions_for_inventory_id = |inventory_id: i64| {
        inventory_definitions.iter().filter(move |id| id.inventory_id == inventory_id)
    };

    //4. One for the latest inventory report
    let latest_inventory_reports_for_inventory = |inventory_id: i64| -> Result<Vec<InventoryReport>, Box<Error>> {
        persistance.get_conn().and_then(|conn| 
            InventoryReport::query(conn)
            .Where(InventoryReport::fields().inventory_id, inventory_id)
            .OrderBy(InventoryReport::fields().timestamp, sqlite_traits::Ordering::Descending)
            .Limit(number)
            .all()
        )
    };

    //5. And one for the crossjoin-filter to get all the products that can be contained in an inventory (either mentioned in InventoryDefinition or ProductReport)
    let possible_product_states_for_inventory = move |inventory_id: i64, inventory_report_id: i64| {
        let product_reports_by_product_id = product_reports_for_inventory_report(inventory_report_id);
        products.iter().filter_map(|p| { //Iterate over all products
            let def = inventory_definitions_for_inventory_id(inventory_id).find(|id| id.product_id == p.id); //Either mentioned in the definition
            let pr = product_reports_by_product_id.get(&(p.id)); //Or the product report

            if def.is_none() && pr.is_none(){ //If it's not mentioned, skip this one
                None
            }else{
                let mut p = ProductState{//Otherwise, let's build a product state to report on
                    product_name: p.name.clone(),
                    product_id: p.id as i64,
                    amount_to_store: def.map(|d| d.amount).unwrap_or(0),
                    amount_stored: pr.map(|pr| pr.amount).unwrap_or(0),
                    maximum: 0
                };
                
                p.maximum = p.amount_to_store.max(p.amount_stored);

                Some(p)
            }
        }).collect::<Vec<ProductState>>()
    };

    Ok(latest_inventory_reports_for_inventory(inventory_id)?.iter().map(|ir| {
        //Build InventoryState from InventoryReport
        InventoryState{
            timestamp: ir.timestamp,
            inventory_id,
            product_states: possible_product_states_for_inventory(inventory_id, ir.id)
        }
    }).collect::<Vec<InventoryState>>())
}

pub fn get_the_latest_inventory_state(persistance: &Persistance, inventory_id: i64) -> Option<InventoryState>{
    get_latest_inventory_states(persistance, inventory_id, 1).ok().and_then(|s| s.into_iter().next())
}

pub fn get_empty_inventory_state(inventory_id: i64) -> InventoryState{
    InventoryState{
        inventory_id,
        timestamp: Local::now().timestamp(),
        product_states: Vec::new()
    }
}

pub fn backfill_empty_product_reports(inventory_state: Vec<InventoryState>) -> Vec<InventoryState>{
    let mut product_ids_mentioned: BTreeSet<i64> = BTreeSet::new();
    let mut products_in_this_set: HashMap<i64, ProductState> = HashMap::new();

    inventory_state.iter().for_each(|invstate| invstate.product_states.iter().for_each(|ref pstate| {if product_ids_mentioned.insert(pstate.product_id as i64){products_in_this_set.insert(pstate.product_id as i64, (*pstate).clone());}} ));

    inventory_state.into_iter().map(|mut invstate| {
        let mut product_ids_in_this_state = BTreeSet::new();
        invstate.product_states.iter().for_each(|pstate| {product_ids_in_this_state.insert(pstate.product_id as i64);});
        product_ids_mentioned.difference(&product_ids_in_this_state).for_each(|pid| {
            let pstate = products_in_this_set.get(pid).unwrap();
            invstate.product_states.push(ProductState{
                product_name: pstate.product_name.clone(),
                product_id: pstate.product_id,
                amount_to_store: pstate.amount_to_store,
                amount_stored: 0,
                maximum: pstate.amount_to_store
            });
        });
        invstate
    }).collect::<Vec<InventoryState>>()
}

pub struct AmountOfProduct{
    pub product_id: i64,
    pub number: i64
}

pub struct ProductsForWeight{
    pub products: Vec<AmountOfProduct>,
    pub weight_left_over: i64
}

//From a weight in grams and a selection of possible products and their maximum availability to Vec<(product_id, number)>, weight_left_over
pub fn get_products_for_weight(weight: i64, products: &Vec<(&Product, i64)>) -> (Vec<(i64, i64)>, i64){

    //println!{"Products: {:?}", products};
    if products.len() == 0 || weight < 1{
        return (Vec::new(),weight)
    }

    let weights = products.iter().map(|(p, _am)|  p.weight_grams).collect::<Vec<i64>>();

    let max_amounts = products.iter().map(|(_p, am)| *am).collect::<Vec<i64>>();

    //println!("Max amounts: {:?}", max_amounts);

    let mut best_guess_weight_diff = weight;

    let mut amounts: Vec<i64> = vec![0;products.len()];

    let mut best_guess = amounts.clone();

    let highest_index = products.len() - 1;

    let mut inferences_count: i64 = 0; //just in case someone enters 0 as a weight FML

    while (amounts[highest_index] <= max_amounts[highest_index]) && inferences_count < 10000000 {
        inferences_count += 1;
        amounts[0] += 1; //Increase amount of Product zero
        for index in 0..highest_index { //Roll over if current column count is greater than total available items of that type
            if amounts[index] > max_amounts[index]{
                amounts[index] = 0;
                amounts[index + 1] += 1;
            }
        }

        if amounts[highest_index] > max_amounts[highest_index] { //Don't run inference past our limits
            break;
        }
        let current_weight_diff = weight - amounts.iter().enumerate().fold(0, |initial, (index, amount)| initial + (amount * weights[index]));

        //println!("Guess: {:?} (wdiff {})", amounts, current_weight_diff);
        if current_weight_diff.abs() < best_guess_weight_diff.abs(){
            best_guess_weight_diff = current_weight_diff;
            //println!{"New best: {}", best_guess_weight_diff};
            best_guess = amounts.clone();
        }
    }

    (best_guess.into_iter().enumerate().map(|(index, amount)|{(products[index].0.id, amount)}).collect(),best_guess_weight_diff)
}

pub fn infer_empty_in(persistance: &Persistance, inventory_id: i64, product_id: i64) -> Result<Option<i64>, Box<Error>>{
    let mut timestamp_amounts = 
        get_latest_inventory_states(persistance, inventory_id, 100)?
        .iter()
        .filter_map(|istate| istate.product_states
            .iter()
            .find(|pstate| pstate.product_id == product_id)
            .and_then(|pstate| Some((istate.timestamp as f64, pstate.amount_stored as f64))))
        .collect::<Vec<(f64, f64)>>();

    let now = Local::now().timestamp();

    loop{
        if timestamp_amounts.len() > 0{
            //Add a timestamp representing the current state
            let last_amount = timestamp_amounts[timestamp_amounts.len() -1].1;
            timestamp_amounts.push((now as f64, last_amount));

            //Let's do a linear regression for this for now
            // a(t) = f0 + f1 * t

            //Method of least squares
            let (avg_time, avg_amount) = timestamp_amounts.iter().fold((0.0, 0.0), |(prev_t, prev_a), (t,a)| (prev_t +t, prev_a + a));

            let f1 = timestamp_amounts.iter().fold(0.0, |prev , (t,a)| prev + ( (t - avg_time) * (a - avg_amount)))
                / timestamp_amounts.iter().fold(0.0, |prev, (t, _a)| prev +  (t - avg_time).powf(2.0));

            let f0 = avg_amount - f1 * avg_time;

            //Ok, now we only need to resolve this
            //a(t) = 0 <==> f0 + f1 * t = 0
            // <==>f1 * t = -f0
            // <==>t = -f0 / f1

            let t = -f0 / f1;

            //Check if the timestamp is AFTER the one we passed in
            if t as i64 > now{
                return Ok(Some(t as i64));
            }else{
                //Let's remove the first timestamp,amount tuple from the array (which should be the one that's the most in the past) and try again
                timestamp_amounts.remove(0);
            }
        }else{
            return Ok(None); //No previous records, but also no errors. We can't make any prediction here, but it's okay
        }
    }

}