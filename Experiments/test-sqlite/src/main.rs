
#[macro_use]
extern crate sqlite_derive;
extern crate sqlite_traits;

use std::path::Path;

use std::error::Error;
use sqlite_traits::Persistance;
use sqlite_traits::dbobject::{DbObject};

#[derive(SqlMacro, Debug)]
struct Test{
    pub id: i64,
    pub name: String,
    pub description: String,
    pub f: f64
} 


fn main() {
    let dbc = Persistance::new(Path::new("db.db"));

    let mut t = Test{
        id: 4,
        name: String::from("Test"),
        description: String::from("Description"),
        f: 4.5
    };

    //Test::demo();

    Test::initialize(&dbc.get_conn().unwrap());
    Test::create(&dbc.get_conn().unwrap(), &mut t);

    let mut t2 = Test::query(dbc.get_conn().unwrap()).Where("f", 4.5).get().unwrap();
    println!("{:?}", t2);
    t2.f = 15.6;

    Test::update(&dbc.get_conn().unwrap(), &t2);

    let mut t3 = Test::query(dbc.get_conn().unwrap()).Where(Test::fields().name, "Test").get().unwrap();
    println!("{:?}", t2);

    Test::delete(&dbc.get_conn().unwrap(), &t3);
    
}
