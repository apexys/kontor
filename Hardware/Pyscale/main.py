import uasyncio as asyncio

import gc
gc.enable()

import logger

logger = logger.logger
logger()

async def gc_task():
    while True:
        await asyncio.sleep(1)
        gc.collect()

asyncio.get_event_loop().call_soon(gc_task())


try:

    import globals

    import os

    globals.configureWifi()

    gc.collect()

    import server
    server.start()

    gc.collect()

    from weightkeeper import weightkeeper_loop
    asyncio.get_event_loop().call_soon(weightkeeper_loop())


    asyncio.get_event_loop().run_forever()
    logger().log("Event loop finished." "MAIN")
    asyncio.get_event_loop().close()

except KeyboardInterrupt:
    print("KeyboardInterrupt")
    import sys
    sys.exit(0)
except BaseException as e:
    logger().log("Critical error: " +str(e) + " (restarting machine)")
    import machine
    machine.reset()