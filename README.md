# kontor

Product tracking platform

## Building

### Server

In order to build the server, you need `rust`, `cargo` and a system C compiler such as `cc` or `MSVC`.

A detailed description of the setup of the rust components is available at [https://rustup.rs/](https://rustup.rs/).

Next, open a terminal in `Server/kontor` and execute `cargo build`.

After completion, the application can be run by executing `cargo run` in the same directory.

The server will open on port 8000 and place a database file at `Server/db.db`.

To run a server that will restart on changes install `watchexec` via `cargo install watchexec` and run `watchexec --exts rs,hbs --restart "cargo run"` in `Server/kontor`.