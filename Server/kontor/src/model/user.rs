use serde_json::Value;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, PartialOrd, Ord)]
#[repr(u8)]
pub enum Usertype{
    Admin = 0,
    User,
    API
}

impl Usertype{
    pub fn from_str(s: &str) -> Usertype{
        match s {
            "Admin" => Usertype::Admin,
            "User" => Usertype::User,
            "API" => Usertype::API,
            _ => unimplemented!()
        }
    }

    pub fn to_str(self) -> &'static str{
        match self{
            Usertype::Admin => "Admin",
            Usertype::User => "User",
            Usertype::API => "API"
        }
    }

    pub fn from_string(s: String) -> Usertype{
        Self::from_str(&s)
    }

    pub fn to_json_object(self) -> Value{
        json!({
            "Admin": self <= Usertype::Admin,
            "User": self <= Usertype::User,
            "API": self == Usertype::API
        })
    }
}

#[derive(SqlMacro, Debug, Serialize)]
pub struct User{
    pub id: i64,
    pub usertype: Usertype,
    pub name: String,
    pub hash: String
}

extern crate bcrypt;



impl User {
    pub fn new(usertype: Usertype, name: &str, password: &str)-> User{
        User{
            id: -1,
            usertype,
            name: String::from(name),
            hash: bcrypt::hash(password, /*bcrypt::DEFAULT_COST*/ 4).unwrap()
        }
    }

    pub fn verify(&self, password: &str)-> bool{
        bcrypt::verify(password, &self.hash).unwrap()
    }

    pub fn set_password(&mut self, password: &str){
        self.hash = bcrypt::hash(password, /*bcrypt::DEFAULT_COST*/ 4).unwrap();
    }
}