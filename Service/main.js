var express = require('express');
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser');
var app = express();
var fs = require("fs");
var handlebars = require("handlebars");
var Connection = require("./connection");
var UserConfiguration = require("./userconfiguration");
var crypto = require("crypto");

//Read config file
var configuration = JSON.parse(fs.readFileSync('configuration.json', "utf-8"));

//Hashmap to store login tokens
var logins = {};

//Static content and parsers
app.use('/static', express.static(__dirname + '/static'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

//Precompile base template
handlebars.partials['base'] = handlebars.compile(fs.readFileSync(__dirname + "/templates/base.hbs", "utf-8"))
handlebars.registerHelper('raw', function(options) {
    return options.fn(this);
});

// GET /
var overviewTemplate = handlebars.compile(fs.readFileSync(__dirname + "/templates/overview.hbs", "utf-8"));
app.get('/', function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        var rules_number = (logins[req.cookies.token].Configuration.get("selection") || []).length;
        res.write(overviewTemplate({rules_number}));
        res.end();
    }else{ //Not logged in, send login form
        res.sendFile(__dirname + "/templates/login.html");
    }
});

//POST /login
app.post('/login',(req, res) => {
    if('username' in req.body && 'password' in req.body){ //Only process request if request complete
        var c = new Connection(configuration['Server-URL']); //Create connection object to buffer our login token
        c.login(req.body.username, req.body.password) //Try logging in
        .then((success) => {
            if(success){//Successful login, we need to store a few things
                var token = crypto.randomBytes(80).toString('hex');
                logins[token] = {
                    Connection: c
                };
                var ucfg = new UserConfiguration(req.body.username);
                ucfg.set("username", req.body.username);
                ucfg.set("password", req.body.password);
                logins[token].Configuration = ucfg;
                res.cookie("token",token);
            }
            res.redirect("/");
        })
        .catch(e => {
            console.log(e);
           res.redirect("/"); 
        });
    }else{
        console.log("failure");
        res.write("login failed");
    }
});

app.get("/logout", function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        logins[req.cookies.token].Configuration.save();
        logins[req.cookies.token].Connection.logout().then(()=> {
            res.clearCookie("token");
            res.redirect("/");
        });
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});


// GET / template
var templateTemplate = handlebars.compile(fs.readFileSync(__dirname + "/templates/template.hbs", "utf-8"));
app.get('/template', function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        res.write(templateTemplate({
            template: logins[req.cookies.token].Configuration.get("templatestring")
        }));
        res.end();
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});

// GET / target
var targetTemplate = handlebars.compile(fs.readFileSync(__dirname + "/templates/target.hbs", "utf-8"));
app.get('/target', function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        var mailconfig = logins[req.cookies.token].Configuration.get("mailconfig");
        res.write(targetTemplate(mailconfig));
        res.end();
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});


// GET / selection
var selectionTemplate = handlebars.compile(fs.readFileSync(__dirname + "/templates/selection.hbs", "utf-8"));
app.get('/selection', function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        logins[req.cookies.token].Connection.get_inventory_definitions().then(inventory_definitions => {
            var selection = logins[req.cookies.token].Configuration.get("selection") || [];
            Promise.all(selection.map(sline => logins[req.cookies.token].Connection.get_inventory_states(sline.inventory_id, 10))).then(istates => {
                selection = selection.map((sline, index) => {sline.state = istates[index][0] || {products:[]}; return sline;});
                var data = selection.map(sline => {
                    var inv = inventory_definitions.find(inv => inv.id == sline.inventory_id);
                    var prod = inv.products.find(prod => prod.id == sline.product_id);
                    return {
                        "inventory-name": inv.name,
                        "inventory-id": inv.id,
                        "product-name": prod.name,
                        "product-id": prod.id,
                        "minimum": sline.minimum,
                        "current": (sline.state.products.find(p => p.product_id == prod.id)|| {amount:0}).amount
                    }
                });
                res.write(selectionTemplate({selection: data}));
                res.end();
            });
        });
        
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});


//POST /preview
app.post("/preview", function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        let previewData = {
            Items: [
                {
                    'inventory-name': 'Test Inventory',
                    'inventory-description': 'A test inventory located somewhere',
                    'inventory-id': 1,
                    'product-name': 'Test Item 1',
                    'product-id': 5,
                    'minimum': 1,
                    'current': 0,
                    'difference': 1
                }
            ]
        };
        var testtemplate = handlebars.compile(req.body.templatestring);
        res.write(testtemplate(previewData));
        res.end();
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});

//POST /template
app.post("/template", function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        logins[req.cookies.token].Configuration.set("templatestring", req.body.templatestring);
        res.redirect("/");
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});

// GET /selection-inventories
app.get('/selection-inventories', function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        logins[req.cookies.token].Connection.get_inventory_definitions().then(invs => {
            res.json(invs);
            res.end();
        });

    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});

app.post('/selection-add-line', function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        var selection = logins[req.cookies.token].Configuration.get('selection') || [];
        selection.push({
            "inventory_id": parseInt(req.body.inventory_id),
            "product_id": parseInt(req.body.product_id),
            "minimum": parseInt(req.body.minimum)
        })
        logins[req.cookies.token].Configuration.set("selection", selection);
        res.redirect("/selection");
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});

app.post('/selection-remove-line', function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        var selection = logins[req.cookies.token].Configuration.get('selection') || [];
        selection = selection.filter(sline => sline.product_id != parseInt(req.body.product_id) || sline.inventory_id != parseInt(req.body.inventory_id));
        logins[req.cookies.token].Configuration.set("selection", selection);
        res.redirect("/selection");
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});

app.post('/target-save-sender', function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        var mailconfig = logins[req.cookies.token].Configuration.get('mailconfig') || {};
        mailconfig.sender_account = req.body.sender_account;
        mailconfig.sender_password = req.body.sender_password;
        logins[req.cookies.token].Configuration.set("mailconfig", mailconfig);
        res.redirect("/target");
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});

app.post('/target-save-target', function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        var mailconfig = logins[req.cookies.token].Configuration.get('mailconfig') || {};
        mailconfig.target_account = req.body.target_account;
        logins[req.cookies.token].Configuration.set("mailconfig", mailconfig);
        res.redirect("/target");
    }else{ //Not logged in, send login form
        res.redirect("/");
    }
});

app.post("/target-test-mail", function(req, res){
    if("token" in req.cookies && req.cookies["token"] in logins){ //Logged in
        var email = require('./email');
        email.sendMail(req.body.account, req.body.password, req.body.account,"Test Email", "This is a test email to verify email sending.").then(m => {
            res.write("Sending mail succeded");
            res.end();
        }).catch(e => {
            res.write("Error sending mail " + e.message);
            res.end();
        });
    }else{ //Not logged in, send login form
        res.redirect("/");
    }

});




app.listen(3000);
