var fs = require('fs');
var UserConfiguration = require('./userconfiguration');
var Connection = require('./connection');
var server_configuration = JSON.parse(fs.readFileSync('configuration.json'));
var handlebars = require('handlebars');
var email = require('./email');

var configs = fs.readdirSync("./configs").map(cfile => new UserConfiguration(cfile));

configs.forEach(config => {
    let selection = config.get("selection") || [];
    if(selection.length > 0){
        console.log("Processing rules for user " + config.get("username"));
        let connection = new Connection(server_configuration["Server-URL"]);
        connection.login(config.get('username'), config.get('password')).then(() => {
            connection.get_inventory_definitions().then(inventory_definitions => {
                Promise.all(selection.map(sline => connection.get_inventory_states(sline.inventory_id, 10))).then(istates => {
                    selection = selection.map((sline, index) => {sline.state = istates[index][0] || {products:[]}; return sline;});
                    var data = selection.map(sline => {
                        var inv = inventory_definitions.find(inv => inv.id == sline.inventory_id);
                        var prod = inv.products.find(prod => prod.id == sline.product_id);
                        return {
                            "inventory-name": inv.name,
                            "inventory-description": inv.description,
                            "inventory-id": inv.id,
                            "product-name": prod.name,
                            "product-id": prod.id,
                            "minimum": sline.minimum,
                            "current": (sline.state.products.find(p => p.product_id == prod.id)|| {amount:0}).amount,
                            "difference": sline.minimum - ((sline.state.products.find(p => p.product_id == prod.id)|| {amount:0}).amount)
                        }
                    }).sort((a,b) => a['inventory-id'] - b['inventory-id']);
                    var template = handlebars.compile(config.get("templatestring"));
                    var text = template({Items: data});
                    var mailconfig = config.get('mailconfig');
                    email.sendMail(mailconfig.sender_account, mailconfig.sender_password,mailconfig.target_account, "Kontor Inventory Report", text);
                    console.log("Email sent for " + config.get("username"));
                });
            });
        });
    }else{
        console.log("User " + config.get("username") + " has nothing to do");
    }
});