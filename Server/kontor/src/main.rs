#![feature(plugin, custom_derive, extern_prelude)]
#![plugin(rocket_codegen)]

extern crate rand;
extern crate rocket;
extern crate rocket_contrib;
use rocket_contrib::Template;


#[macro_use] extern crate serde_derive;

#[macro_use] extern crate simple_error;
use simple_error::SimpleError;

extern crate sqlite_traits;
#[macro_use] extern crate sqlite_derive;

extern crate serde;
#[macro_use] extern crate serde_json;
use serde_json::Value;


extern crate uuid;

mod model;

mod login_users;
use login_users::{AdminUser, NormalUser, APIUser};

use rocket::response::NamedFile;
use std::path::Path;
use std::path::PathBuf;
use rocket::response::Redirect;
use rocket::request::Form;
use rocket::http::uri::URI;
use rocket_contrib::Json;
use rocket::http::{Cookie, Cookies};
use rocket::State;

extern crate chrono;
use chrono::prelude::*;

use model::Persistance;
use model::user::{User, Usertype};
use model::product::Product;
use model::dbobject::DbObject;
use model::inventory::{Inventory, InventoryDefinition};
use model::report::{InventoryReport, ProductReport, WeightReport, SensorType};

use std::error::Error;

mod messages;

mod state_deduction;


use std::collections::HashMap;

#[get("/", rank = 2)]
fn login_form() -> Option<NamedFile> {
    NamedFile::open(Path::new("templates/login.html")).ok()
}

#[get("/favicon.ico")]
fn favicon() -> Option<NamedFile>{
    NamedFile::open(Path::new("static/favicon.ico")).ok()
}

#[get("/")]
fn overview(l: Usertype, persistance: State<Persistance>) ->  Result<Template, Box<Error>> {

    let all_inventories = Inventory::query(persistance.get_conn()?).all()?;
    let invs = all_inventories.iter().map(|inv| (inv, state_deduction::get_the_latest_inventory_state(&persistance, inv.id)));

    Ok(Template::render("overview", json!({
        "Usertype": l.to_json_object(),
        "Inventories": invs.map(|(inv, inv_state)| json!({
            "name": &inv.name,
            "description": &inv.description,
            "timestamp": inv_state.iter().map(|inv_state| Local.timestamp(inv_state.timestamp,0).format("%d.%m.%Y %H:%M:%S").to_string()).next(),
            "products": inv_state.map(|inv_state| inv_state.product_states)
        })).collect::<Value>()
    })))
}

#[get("/static/<file..>")]
fn files(file: PathBuf) -> Result<NamedFile, String>  {
    let path = Path::new("static/").join(file);
    NamedFile::open(&path).map_err(|_| format!("Bad path: /static/{:?}", path))
}

#[get("/clear_message")]
fn clear_message(mut cookies: Cookies) -> &'static str{
        cookies.remove(Cookie::named("message"));
        "message cleared"
}

macro_rules! set_message_and_redirect {
    ($message:path, $path:expr, $cookies:expr) => {{
        let mut c = Cookie::new("message",$message);
        c.set_path("/");
        $cookies.add(c);
        Ok(Redirect::to($path))
    }};
}

#[derive(FromForm)]
struct LoginCredentials{
    username: String,
    password: String
}

#[post("/login", data="<lc>")]
fn login(lc: Form<LoginCredentials>, mut cookies: Cookies, persistance: State<Persistance>) -> Result<Redirect, Box<Error>>{
    let creds:&LoginCredentials = lc.get();
    if let Ok(token) = persistance.log_in(&creds.username, &creds.password){
        cookies.add_private(Cookie::new("token", token));
    }
    let mut redirect_str = String::from("/");
    if let Some(redirect_to) = cookies.get("redirect_to"){
        redirect_str = String::from(redirect_to.value());
        println!{"Redirect cookie: {}", &redirect_str};
    }
    cookies.remove(Cookie::named("redirect_to"));
    Ok(Redirect::to(&redirect_str))
}


#[get("/logout")]
fn logout(_l: Usertype, mut cookies: Cookies, persistance: State<Persistance>) -> Result<Redirect, Box<Error>>{
    if let Some(cookie) = cookies.get_private("token"){
        persistance.log_out(cookie.value())?;
        cookies.remove_private(cookie);
    }
    Ok(Redirect::to("/"))
}


#[get("/usersettings")]
fn usersettings(_admin: AdminUser,u: Usertype, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let users = User::query(persistance.get_conn()?).all()?.iter().map(|u| json!({
        "id": u.id, 
        "name": u.name, 
        "Usertype": u.usertype.to_json_object()
    })).collect::<Value>();
    Ok(Template::render("usersettings", json!({
        "Usertype": u.to_json_object(),
        "users": users
    })))
}

#[get("/settings")]
fn settings(u: User) -> Template{
    Template::render("settings", json!({
        "Usertype": u.usertype.to_json_object(),
        "name": u.name
    }))
}

#[derive(FromForm)]
struct UsernameUpdate{
    username: String
}

#[post("/update_username", data="<data>")]
fn update_username(mut u: User, data: Form<UsernameUpdate>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let data = data.get();
    let users = User::query(persistance.get_conn()?).all()?;
    if data.username.len() > 1 && users.iter().all(|ou| ou.name != data.username){
        u.name = data.username.clone();
        User::update(&persistance.get_conn()?,&u)?;
        set_message_and_redirect!(messages::USERNAME_CHANGED, "/", cookies)
    }else{
        set_message_and_redirect!(messages::USERNAME_TAKEN, "/settings", cookies)
    }
}

#[derive(FromForm)]
struct PasswordUpdate{
    password: String
}

#[post("/update_password", data="<data>")]
fn update_password(mut u: User, data: Form<PasswordUpdate>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let data = data.get();
    if data.password.len() > 0{
        u.set_password(&data.password);
        User::update(&persistance.get_conn()?,&u)?;
        set_message_and_redirect!(messages::PASSWORD_CHANGED, "/", cookies)
    }else{
        set_message_and_redirect!(messages::PASSWORD_TOO_SHORT, "/settings", cookies)
    }
}

#[derive(FromForm)]
struct UserCreation{
    usertype: String,
    username: String,
    password: String
}

#[post("/admin_create_user", data="<data>")]
fn admin_create_user(_admin: AdminUser, data: Form<UserCreation>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let data = data.get();
    let users = User::query(persistance.get_conn()?).all()?;
    if data.username.len() > 1 && users.iter().all(|ou| ou.name != data.username){
        if data.password.len() > 0{
            User::create(&persistance.get_conn()?, &mut User::new(Usertype::from_str(&data.usertype), &data.username, &data.password))?;
            set_message_and_redirect!(messages::USER_CREATED, "/usersettings", cookies)
        }else{
            set_message_and_redirect!(messages::PASSWORD_TOO_SHORT, "/usersettings", cookies)
        }
    }else{
        set_message_and_redirect!(messages::USERNAME_TAKEN, "/usersettings", cookies)
    }
}

#[derive(FromForm)]
struct AdminUsernameUpdate{
    userid: i32,
    username: String
}
#[post("/admin_update_username", data="<data>")]
fn admin_update_username(_admin: AdminUser, data: Form<AdminUsernameUpdate>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let data = data.get();
    let mut u = User::query(persistance.get_conn()?).Where(User::fields().id, data.userid).get().ok_or(SimpleError::new("User not in db"))?;
    let users = User::query(persistance.get_conn()?).all()?;
    if data.username.len() > 1 && users.iter().all(|ou| ou.name != data.username){
        u.name = data.username.clone();
        User::update(&persistance.get_conn()?,&u)?;
        set_message_and_redirect!(messages::USERNAME_CHANGED, "/usersettings", cookies)
    }else{
        set_message_and_redirect!(messages::USERNAME_TAKEN, "/usersettings", cookies)
    }
}

#[derive(FromForm)]
struct AdminPasswordUpdate{
    userid: i32,
    password: String
}
#[post("/admin_update_password", data="<data>")]
fn admin_update_password(_admin: AdminUser, data: Form<AdminPasswordUpdate>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let data = data.get();
    let mut u = User::query(persistance.get_conn()?).Where(User::fields().id, data.userid).get().ok_or(SimpleError::new("User not in db"))?;
    if data.password.len() > 0{
        u.set_password(&data.password);
        User::update(&persistance.get_conn()?,&u)?;
        set_message_and_redirect!(messages::PASSWORD_CHANGED, "/usersettings", cookies)
    }else{
        set_message_and_redirect!(messages::PASSWORD_TOO_SHORT, "/usersettings", cookies)
    }
}

#[get("/products", rank=2)]
fn products(_user: NormalUser, u: Usertype, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let products = Product::query(persistance.get_conn()?).all()?;
    Ok(Template::render("products", json!({
        "Usertype": u.to_json_object(),
        "products": products
    })
    ))
}

#[derive(FromForm, Serialize, Clone)]
struct ProductCreation{
    name: String,
    supplier: String,
    weight_grams: String
}

#[get("/products/new", rank=2)]
fn products_new_redirect(_user: NormalUser, u: Usertype, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let inventories = Inventory::query(persistance.get_conn()?).all()?.into_iter().map(|p| json!({"id": p.id, "name": p.name})).collect::<Vec<Value>>();
    Ok(Template::render("product", json!({
        "Usertype": u.to_json_object(),
        "Inventories": inventories
    })
    ))
}

#[get("/products/new?<form>")]
fn products_new(_user: NormalUser, u: Usertype, form: Option<ProductCreation>, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let inventories = Inventory::query(persistance.get_conn()?).all()?.into_iter().map(|p| json!({"id": p.id, "name": p.name})).collect::<Vec<Value>>();    
    Ok(Template::render("product", json!({
        "Usertype": u.to_json_object(),
        "Product": form,
        "Inventories": inventories
    })
    ))
}

#[post("/products/new", data="<data>")]
fn products_new_create(_user: NormalUser, data: Form<ProductCreation>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let mut data = data.get().clone();
    let retry_uri = format!{"/products/new?name={}&supplier={}&weight_grams={}", URI::percent_encode(&data.name), URI::percent_encode(&data.supplier), URI::percent_encode(&data.weight_grams)};
    if Product::query(persistance.get_conn()?).Where(Product::fields().name, data.name.clone()).get().is_none(){
        if data.weight_grams.ends_with('g') {
            data.weight_grams.pop();
        }
        if let Ok(weight) = data.weight_grams.parse::<i64>(){
            let mut p = Product::new(data.name, data.supplier, weight);
            Product::create(&persistance.get_conn()?, &mut p)?;
            set_message_and_redirect!(messages::PRODUCT_CREATED, "/products", cookies)
        }else{
            set_message_and_redirect!(messages::PRODUCT_WEIGHT_WRONG, &retry_uri, cookies)
        }  
    }else{
        set_message_and_redirect!(messages::PRODUCT_ALREADY_EXISTS, &retry_uri, cookies)
    }
}

#[get("/products/<id>")]
fn products_edit(_user: NormalUser, u: Usertype, id: i64, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let product = Product::query(persistance.get_conn()?).Where(Product::fields().id, id).get().ok_or(SimpleError::new("Product not in db"))?;
    Ok(Template::render("product", json!({
            "Usertype": u.to_json_object(),
            "Product": product
        })
    ))
}

#[post("/products/update/<id>", data="<data>")]
fn products_update(_user: NormalUser, id: i64, data: Form<ProductCreation>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let mut data = data.get().clone();
    let mut product = Product::query(persistance.get_conn()?).Where(Product::fields().id, id).get().ok_or(SimpleError::new("Product not in db"))?;
    product.name = data.name;
    product.supplier = data.supplier;
    if data.weight_grams.ends_with('g') {
            data.weight_grams.pop();
    }
    if let Ok(weight) = data.weight_grams.parse::<i64>(){
        product.weight_grams = weight;
        Product::update(&persistance.get_conn()?, &product)?;
        set_message_and_redirect!(messages::PRODUCT_UPDATED, "/products", cookies)
    }else{
        set_message_and_redirect!(messages::PRODUCT_WEIGHT_WRONG, &format!{"/products/{}", id}, cookies)
    }  
}

#[post("/products/delete/<id>")]
fn products_delete(_user: NormalUser, id: i64, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let product = Product::query(persistance.get_conn()?).Where(Product::fields().id, id).get().ok_or(SimpleError::new("Product not in db"))?;
    Product::delete(&persistance.get_conn()?, &product)?;
    set_message_and_redirect!(messages::PRODUCT_DELETED, "/products", cookies)
}

#[get("/inventories", rank=2)]
fn inventories(_user: NormalUser, u: Usertype, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let inventories = Inventory::query(persistance.get_conn()?).all()?;
    Ok(Template::render("inventories", json!({
        "Usertype": u.to_json_object(),
        "inventories": inventories
    })
    ))
}

#[get("/inventories_json")]
fn inventories_json(_user: APIUser, persistance: State<Persistance>) -> Result<Json<Vec<Inventory>>, Box<Error>>{
    let inventories = Inventory::query(persistance.get_conn()?).all()?;
    Ok(Json(inventories))
}

#[get("/inventory_definitions_json")]
fn inventory_definitions_json(_user: APIUser, persistance: State<Persistance>) -> Result<Json<Value>, Box<Error>>{
    let inventories = Inventory::query(persistance.get_conn()?).all()?;
    let inventory_definitions = InventoryDefinition::query(persistance.get_conn()?).all()?;
    let products = Product::query(persistance.get_conn()?).map_all()?;

    let data = inventories.iter().map(|inv| json!({
        "id": inv.id,
        "name": inv.name,
        "description": inv.description,
        "products": inventory_definitions.iter()
                        .filter(|idef| idef.inventory_id == inv.id)
                        .map(|idef| (idef, products.get(&idef.product_id)))
                        .map(|(idef, prod)| json!({
                            "id": idef.product_id,
                            "amount": idef.amount,
                            "name": prod.map(|p| p.name.clone()),
                            "supplier": prod.map(|p| p.supplier.clone())
                        }))
                        .collect::<Value>()
    })).collect::<Value>();
    Ok(Json(data))
}

#[get("/inventory_states_json/<id>/<max>")]
fn inventory_states_json(_user: APIUser, id: i64, max: i64, persistance: State<Persistance>) -> Result<Json<Value>, Box<Error>>{
    let reports = InventoryReport::query(persistance.get_conn()?)
        .Where(InventoryReport::fields().inventory_id, id)
        .OrderBy(InventoryReport::fields().timestamp, sqlite_traits::Ordering::Descending)
        .Limit(max).all()?.iter().map(|ir| json!({
            "timestamp": ir.timestamp,
            "products": persistance.get_conn().ok().map(|conn| 
                                                            ProductReport::query(conn)
                                                                .Where(ProductReport::fields().report_id, ir.id)
                                                                .OrderBy(ProductReport::fields().product_id, sqlite_traits::Ordering::Ascending)
                                                                .all().ok()
                                                                .map(|prs| 
                                                                    prs.iter().map(|pr| json!({
                                                                        "product_id": pr.product_id,
                                                                        "amount": pr.amount
                                                                    }))
                                                                    .collect::<Value>() 
                                                                ))
        })).collect::<Value>();

    Ok(Json(reports)) 
}

#[derive(FromForm, Serialize, Clone)]
struct InventoryCreation{
    name: String,
    description: String
}

#[get("/inventories/new", rank=2)]
fn inventories_new_redirect(_user: NormalUser, u: Usertype) -> Template{
    Template::render("inventory", json!({
        "Usertype": u.to_json_object(),
        "Inventory": false
    })
    )
}

#[get("/inventories/new?<form>")]
fn inventories_new(_user: NormalUser, u: Usertype, form: Option<InventoryCreation>) -> Result<Template, Box<Error>>{
    Ok(Template::render("inventory", json!({
        "Usertype": u.to_json_object(),
        "Inventory": form
    })
    ))
}

#[post("/inventories/new", data="<data>")]
fn inventories_new_create(_user: NormalUser, data: Form<InventoryCreation>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let data = data.get().clone();
    let retry_uri = format!{"/inventories/new?name={}&description={}", URI::percent_encode(&data.name), URI::percent_encode(&data.description)};
    if Inventory::query(persistance.get_conn()?).Where(Inventory::fields().name, data.name.clone()).get().is_none(){
        let mut inv = Inventory::new(data.name, data.description);
        Inventory::create(&persistance.get_conn()?, &mut inv)?;
        set_message_and_redirect!(messages::INVENTORY_CREATED, &format!{"/inventories/{}", inv.id}, cookies) 
    }else{
        set_message_and_redirect!(messages::INVENTORY_EXISTS, &retry_uri, cookies)
    }
}

#[get("/inventories/<id>")]
fn inventories_edit(_user: NormalUser, u: Usertype, id: i64, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let inventory = Inventory::query(persistance.get_conn()?).Where(Inventory::fields().id, id).get().ok_or(SimpleError::new("Inventory not in db"))?;
    Ok(Template::render("inventory", json!({
            "Usertype": u.to_json_object(),
            "Inventory": inventory
        })
    ))
}

#[post("/inventories/update/<id>", data="<data>")]
fn inventories_update(_user: NormalUser, id: i64, data: Form<InventoryCreation>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let data = data.get().clone();
    let mut inventory = Inventory::query(persistance.get_conn()?).Where(Inventory::fields().id, id).get().ok_or(SimpleError::new("Inventory not in db"))?;
    inventory.name = data.name;
    inventory.description = data.description;
    Inventory::update(&persistance.get_conn()?, &inventory)?;
    set_message_and_redirect!(messages::INVENTORY_UPDATED, "/inventories", cookies)

}

#[post("/inventories/delete/<id>")]
fn inventories_delete(_user: NormalUser, id: i64, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let inventory = Inventory::query(persistance.get_conn()?).Where(Inventory::fields().id, id).get().ok_or(SimpleError::new("Inventory not in db"))?;
    Inventory::delete(&persistance.get_conn()?, &inventory)?;
    set_message_and_redirect!(messages::INVENTORY_DELETED, "/inventories", cookies)
}

#[get("/inventories/link/<id>")]
fn inventories_link(_user: NormalUser, u: Usertype, id: i64, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let product_names: Vec<(String, i64)> = Product::query(persistance.get_conn()?).all()?.into_iter().map(|p| (p.name, p.id)).collect();

    /*
        I LEAVE THIS HERE AS A WARNING OF WHAT THE CODE COULD HAVE LOOKED LIKE
    let definitions = InventoryDefinition::query(persistance.get_conn()?).Where(InventoryDefinition::fields().inventory_id, id).all()?.into_iter()
        .map(|d| {
            persistance.get_conn().and_then(|conn| 
                Product::query(conn).Where(Product::fields().id, d.product_id).get().map(|p|
                    json!({
                        "product_id": d.product_id,
                        "product_name": p.name,
                        "minimum": d.amount
                    })
                ).ok_or(Box::new(SimpleError::new("Product not in db")))
            )
    }).collect::<Result<Vec<Value>, Box<Error>>>()?;
    */

    //Still not pretty
    let definitions: Vec<Value> = InventoryDefinition::query(persistance.get_conn()?).Where(InventoryDefinition::fields().inventory_id, id).all()?.into_iter().map(|d|
        json!({
            "definition_id": d.id,
            "product_id": d.product_id,
            "product_name": product_names.iter().find(|p| p.1 == d.product_id).unwrap_or(&(String::from("Error!"),0)).0,
            "minimum": d.amount
        })
    ).collect();

    let jsdefs = json!({"Definitions ": definitions}).to_string();

    println!{"{}", jsdefs};

    Ok(Template::render("inventory_description", json!({
            "Usertype": u.to_json_object(),
            "Products": product_names.iter().map(|x| x.0.clone()).collect::<Vec<String>>(),
            "Definitions": definitions,
            "Inventory_Id": id
        })
    ))
}

#[derive(FromForm, Serialize, Clone)]
struct InventoryDefinitionCreation{
    inventory_id: i64,
    minimum: i64,
    product_name: String,
}

#[post("/inventories/link/create", data="<data>")]
fn inventories_link_create(_user: NormalUser, data: Form<InventoryDefinitionCreation>, persistance: State<Persistance>) -> Result<Redirect, Box<Error>>{
    let data = data.get().clone();
    let product_id = Product::query(persistance.get_conn()?).Where(Product::fields().name, data.product_name).get().ok_or(SimpleError::new("Product not in db"))?.id;
    let inventory_id = Inventory::query(persistance.get_conn()?).Where(Inventory::fields().id, data.inventory_id).get().ok_or(SimpleError::new("Inventory not in db"))?.id; //This is only validation, if this request fails, the user changed the URL of the iframe

    //If a definition with both ids already exists, just update the amount, else create it
    if let Some(mut inventory_definition) = InventoryDefinition::query(persistance.get_conn()?).Where(InventoryDefinition::fields().inventory_id, inventory_id).Where(InventoryDefinition::fields().product_id, product_id).get(){
        inventory_definition.amount = data.minimum;
        InventoryDefinition::update(&persistance.get_conn()?, &inventory_definition)?;
    }else{
        let mut inventory_definition = InventoryDefinition::new(inventory_id, product_id, data.minimum);
        InventoryDefinition::create(&persistance.get_conn()?, &mut inventory_definition)?;
    }
    Ok(Redirect::to( &format!{"/inventories/link/{}", inventory_id}))
}

#[post("/inventories/link/delete/<id>")]
fn inventories_link_delete(_user: NormalUser, id: i64, persistance: State<Persistance>) -> Result<Redirect, Box<Error>>{
    let inventory_definition = InventoryDefinition::query(persistance.get_conn()?).Where(InventoryDefinition::fields().id, id).get().ok_or(SimpleError::new("InventoryDefinition not in db"))?;
    InventoryDefinition::delete(&persistance.get_conn()?, &inventory_definition)?;
    Ok(Redirect::to( &format!{"/inventories/link/{}", inventory_definition.inventory_id}))
}

#[get("/reports", rank=2)]
fn reports(_user: NormalUser, u: Usertype, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let products = Product::query(persistance.get_conn()?).map_all()?;
    let inventories = Inventory::query(persistance.get_conn()?).map_all()?;
    let inventory_reports = InventoryReport::query(persistance.get_conn()?).OrderBy(InventoryReport::fields().timestamp, sqlite_traits::Ordering::Descending).all()?;
    let product_reports = ProductReport::query(persistance.get_conn()?).all()?;

    let reports = inventory_reports.iter().map(|ir| json!({
        "id": ir.id,
        "timestamp": Local.timestamp(ir.timestamp,0).format("%d.%m.%Y %H:%M:%S").to_string(),
        "inventory": inventories.get(&ir.inventory_id),
        "products": product_reports.iter()
                            .filter(|pr| pr.report_id == ir.id)
                            .map(|pr| json!({
                                 "product_id": pr.product_id,
                                 "amount": pr.amount,
                                 "product_name": products.get(&pr.product_id).map(|p| p.name.clone())
                            })).collect::<Value>()
    })).collect::<Value>();
 

    Ok(Template::render("reports", json!({
            "Usertype": u.to_json_object(),
            "reports": reports
        })
    ))
}

#[get("/reports/new", rank=2)]
fn reports_new(_user: NormalUser, u: Usertype, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let products = Product::query(persistance.get_conn()?).all()?.into_iter().map(|p| (p.name)).collect::<Vec<String>>();
    let inventories = Inventory::query(persistance.get_conn()?).all()?.into_iter().map(|p| (p.name)).collect::<Vec<String>>();

    Ok(Template::render("report", json!({
        "Usertype": u.to_json_object(),
        "Products": products,
        "Inventories": inventories
    }))
    )
}

#[derive(FromForm, Debug, Clone)]
struct ReportCreation{
    inventory_name: String,
    productsjson: String
}

#[post("/reports/create", data="<data>")]
fn reports_new_create(_user: NormalUser, data: Form<ReportCreation>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let data = data.get();
    let inventory = Inventory::query(persistance.get_conn()?).Where(Inventory::fields().name, data.inventory_name.clone()).get().ok_or(SimpleError::new("Inventory not in db"))?;
    let mut inventory_report = InventoryReport::new(Local::now().timestamp(), inventory.id, SensorType::Manual);
    InventoryReport::create(&persistance.get_conn()?, &mut inventory_report)?;

    //Really not the most elegant solution, and I should probably do something if the insertion of a single product report fails and the rest is working fine, but it's okay for now

    let jsondata: Value = serde_json::from_str(&data.productsjson)?; //Convert from string to Value
    jsondata.as_object() //Value -> Option<Value::Object>
        .ok_or(SimpleError::new("Faulty report data!"))  //Option<Value::Object> -> Result<Value::Object, Box<Error>>
        .map(|o| {o.iter().map(|(k,v)| -> Result<ProductReport, Box<Error>> { //Do the actual insertion for all contained products if the value is Ok
            let p: &Persistance = &persistance;
            let c: sqlite_traits::DbConnection = p.get_conn()?;
            let product = Product::query(c).Where(Product::fields().name, k.clone()).get().ok_or(SimpleError::new("Inventory not in db"))?;
            let mut pr = ProductReport::new(inventory_report.id, product.id, v.as_i64().ok_or(SimpleError::new("Faulty report data!"))?);
            ProductReport::create(&persistance.get_conn()?, &mut pr)?;
            Ok(pr)
        })
        .fold(Ok(ProductReport::new(0,0,0)), |init, value| {init.and(value)}) //The insertion yields an iterator of Results which get coalesced into the first result or a dummy ProductReport
    })??; //Two questionmarks: First one asks if we got a Value::Object in the first place, second one asks if there were any errors iterating over it

    //Oh, before I forget, I think this one will actually iterate over all values in the object, but only the first error will get reported.
    //Reports shouldn't fail anyways, since all the data is generated by the program itself and the user only has limited control over which data to include
    //Still, better safe than sorry

     set_message_and_redirect!(messages::REPORT_CREATED, "/reports", cookies)
}

//TODO: Actually implement this
#[get("/graph/inventory/<id>")]
fn inventory_graph(_user: NormalUser, _u: Usertype, id: i64, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let states = state_deduction::backfill_empty_product_reports(state_deduction::get_latest_inventory_states(&persistance, id, 10)?);
    let _h = 360; let __w = 480;

    let mut max_product_amount = 0;
    states.iter().for_each(|invstate| invstate.product_states.iter().for_each(|prodstate| {if prodstate.maximum > max_product_amount { max_product_amount = prodstate.maximum } }));

    let _report_count = states.len();

    Ok(Template::render("inventory_graph", json!({
        "states_count": states.len(),
        "states": states
    })))
}

#[get("/sensors", rank=2)]
fn sensors(_user: NormalUser, u: Usertype, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let reports = WeightReport::query(persistance.get_conn()?).OrderBy(WeightReport::fields().timestamp, sqlite_traits::Ordering::Descending).all()?;
    let inventories = Inventory::query(persistance.get_conn()?).map_all()?;

    Ok(Template::render("sensors", json!({
        "Usertype": u.to_json_object(),
        "reports": reports.iter().map(|report| json!({
            "id": report.id,
            "timestamp": Local.timestamp(report.timestamp,0).format("%d.%m.%Y %H:%M:%S").to_string(),
            "inventory_id": report.inventory_id,
            "inventory_name": inventories.get(&report.inventory_id).map(|inv| inv.name.clone()),
            "weight": report.weight_grams
        })).collect::<Value>()
    })))
}

#[get("/sensors/new", rank=2)]
fn sensors_new(_user: NormalUser, u: Usertype, persistance: State<Persistance>) -> Result<Template, Box<Error>>{
    let inventories = Inventory::query(persistance.get_conn()?).all()?.into_iter().map(|p| json!({"id": p.id, "name": p.name})).collect::<Vec<Value>>();

    Ok(Template::render("sensor_report", json!({
        "Usertype": u.to_json_object(),
        "Inventories": inventories
    }))
    )
}

#[get("/sensors/latest/<inv_id>")]
fn sensors_latest_report_for_inventory(_user: APIUser, u: Usertype, inv_id: i64, persistance: State<Persistance>)-> Result<Json<Option<WeightReport>>, Box<Error>>{
    let report = WeightReport::query(persistance.get_conn()?).Where(WeightReport::fields().inventory_id, inv_id).OrderBy(WeightReport::fields().timestamp, sqlite_traits::Ordering::Descending).get();
    if report.is_none(){
        Ok(Json(Some(WeightReport::new(Local::now().timestamp(), inv_id, 0)))) //No report there -> report we have no weight on sensor
    }else{
        Ok(Json(report)) //Report there
    }
}

#[derive(FromForm, Debug, Clone)]
struct SensorReportCreation{
    inventory_id: String,
    weight_grams: String
}

#[post("/sensors/report", data="<data>")]
fn sensors_report(_user: APIUser, data: Form<SensorReportCreation>, persistance: State<Persistance>, mut cookies: Cookies) -> Result<Redirect, Box<Error>>{
    let mut data = data.get().clone();
    println!("Inventory Id: '{}'", data.inventory_id);
    let inv_id = data.inventory_id.parse::<i64>()?;
    let inventory = Inventory::query(persistance.get_conn()?).Where(Inventory::fields().id, inv_id).get().ok_or(SimpleError::new("Inventory not in db"))?;
    if data.weight_grams.ends_with('g') {
            data.weight_grams.pop();
    }
    let weight = data.weight_grams.parse::<i64>()?;
    


    //Create automated inventory report

    //Get all products that may be stored in this inventory
    let products = Product::query(persistance.get_conn()?).map_all()?;
    let invdef = InventoryDefinition::query(persistance.get_conn()?).Where(InventoryDefinition::fields().inventory_id, inventory.id).all()?;
    let products_in_inventory = invdef.iter().filter_map(|def| products.get(&def.product_id).map(|p| (p, def.amount))).collect::<Vec<(&Product, i64)>>();

    //Get current product state of the inventory
    let mut products_to_amounts =state_deduction::get_the_latest_inventory_state(&persistance, inventory.id)
        .unwrap_or(state_deduction::get_empty_inventory_state(inventory.id))
        .product_states.iter()
        .fold(HashMap::new(), |mut map, pstate| {map.insert(pstate.product_id, pstate.amount_stored); map}); 

/*
    //Case 1: There's a prior weight report
    if let Some(report) = WeightReport::query(persistance.get_conn()?).OrderBy(WeightReport::fields().timestamp, sqlite_traits::Ordering::Descending).Limit(1).get(){
        //Get the weight difference
        let prior_weight = report.weight_grams;
        let weight_diff = weight - prior_weight;
        
        let (inference_result, inference_weight_diff) = state_deduction::get_products_for_weight(weight_diff.abs(), &products_in_inventory);
        println!("Inference of {}g of weight for inventory id {} resulted in {} items and {}g left-over weight", weight_diff, inventory.id, inference_result.len(), inference_weight_diff);

        if weight_diff > 0 { //Weight was added, so there's more products than before
            inference_result.iter().for_each(|(product_id, amount)| {
                let previous_amount =  *products_to_amounts.get(product_id).unwrap_or(&0) as i64;
                products_to_amounts.insert(*product_id, previous_amount + amount);
            });
        }else{ //Weight was removed, so there's less products than before
            //Check if we actually have enough of all products to 
            if inference_result.iter().all(|(product_id, amount)| products_to_amounts.get(product_id).map(|stored| stored > amount).unwrap_or(false)) {
                //We have enough of each product, time to reduce!
                inference_result.iter().for_each(|(product_id, amount)|{
                    let previous_amount =  *products_to_amounts.get(product_id).unwrap_or(&0) as i64;
                    products_to_amounts.insert(*product_id, previous_amount - amount);
                });
            }else{
                //The inference result told us to remove products that aren't there.
                //That means the inference went wrong.
                //To try and save the situation, we can derive a completely new report from the full weight
                let (inference_result, _inference_weight_diff) = state_deduction::get_products_for_weight(weight, &products_in_inventory);
                println!("Inference of {}g of weight for inventory id {} resulted in {} items and {}g left-over weight", weight_diff, inventory.id, inference_result.len(), inference_weight_diff);
                products_to_amounts = inference_result.iter().fold(HashMap::new(), |mut map, (product_id, amount)| {map.insert(*product_id, *amount); map});
            }
        }
    }else{*/
        //It's the first product report! We can basically do whatever here! Yay!
        let (inference_result, inference_weight_diff) = state_deduction::get_products_for_weight(weight, &products_in_inventory);
        println!("Inference of {}g of weight for inventory id {} resulted in {} items and {}g left-over weight", weight, inventory.id, inference_result.len(), inference_weight_diff);
        products_to_amounts = inference_result.iter().fold(HashMap::new(), |mut map, (product_id, amount)| {map.insert(*product_id, *amount); map});
    //}

    //Create and save all three reports
    let conn = persistance.get_conn()?;
    let mut w = WeightReport::new(Local::now().timestamp(), inventory.id, weight);
    WeightReport::create(&conn, &mut w)?;
    let mut invrep = InventoryReport::new(Local::now().timestamp(), inventory.id, SensorType::Sensor);
    InventoryReport::create(&conn, &mut invrep)?;
    products_to_amounts.iter().map(|(&product_id, &amount)| {
        let mut prodrep = ProductReport::new(invrep.id, product_id, amount);
        ProductReport::create(&conn, &mut prodrep)
    }).fold(Ok(()), |r1, r2| r1.and(r2))?;

    set_message_and_redirect!(messages::SENSOR_REPORT_CREATED, "/sensors", cookies)
}

fn main() {
    rocket::ignite()
    .manage(model::Persistance::new())
    .mount("/", routes![
        favicon, //Icon 
        login_form, overview, files, //Login, main page, static files
        clear_message, //Message clearing for the popup system
        login, logout,  //POST-Actions for login and logout
        settings, update_username, update_password, //Settings-page and associated actions
        usersettings, admin_create_user, admin_update_username, admin_update_password, //User-Settings-Page and associated actions
        products, products_new, products_new_redirect, products_new_create, products_edit, products_update, products_delete, //Product-related actions
        inventories, inventories_json, inventories_new, inventories_new_redirect, inventories_new_create, inventories_edit, inventories_update, inventories_delete, //Inventory-related actions
        inventory_definitions_json, inventory_states_json,
        inventories_link, inventories_link_create /*create is also update*/, inventories_link_delete, //Inventory-linking related actions
        reports, reports_new, reports_new_create,
        sensors, sensors_new, sensors_report, sensors_latest_report_for_inventory
    ])
    .attach(Template::fairing()) //Templating-system
    //Ad-hoc fairing that forwards to login from any restricted path hit while not logged in
    .attach(login_users::get_logon_redirect_fairing())
    .launch();
}
