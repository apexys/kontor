from logger import logger
import uasyncio as asyncio

from globals import scale, ap, sta, settings, configureWifi, is_sta_connected
from connection import Connection
import gc
import os

from helpers import decode_formdata

async def ok(socket, query):
    yield from socket.awrite("HTTP/1.1 OK\r\n\r\n")

async def writeReading(socket):
    yield from socket.awrite("HTTP/1.1 OK\r\n\r\n")
    result = scale.get_reading()
    yield from socket.awrite(str(result))

async def writeSSEWeight(socket):
    yield from socket.awrite("HTTP/1.1 OK\r\n")
    yield from socket.awrite("Content-Type: text/event-stream\r\n\r\n")
    while True:
        result = scale.get_reading()
        yield from socket.awrite("event: weight\n")
        yield from socket.awrite("data: " + str(result))
        yield from socket.awrite("\n\n")
        await asyncio.sleep_ms(500)

async def writeTare(socket):
    yield from socket.awrite("HTTP/1.1 OK\r\n\r\n")
    for i in range(0, 20):
        scale.tare()
        yield from asyncio.sleep_ms(500)
    settings.save()
    yield from socket.awrite("tared")

def get_ifconfig_as_env():
    env = {}
    if ap.active():
        ifcfg = ap.ifconfig()
        env["AP_IP"] = str(ifcfg[0])
        env["AP_Netmask"] = str(ifcfg[1])
        env["AP_Gateway"] = str(ifcfg[2])
        env["AP_DNS"] = str(ifcfg[3])
    if sta.active():
        ifcfg = sta.ifconfig()
        env["STA_IP"] = str(ifcfg[0])
        env["STA_Netmask"] = str(ifcfg[1])
        env["STA_Gateway"] = str(ifcfg[2])
        env["STA_DNS"] = str(ifcfg[3])
    return env

async def writeWifiStatus(writer):
    yield from writer.awrite("HTTP/1.1 OK\r\n\r\n")
    ifcfg = ap.ifconfig()
    result = "AP:\n"
    result += "\tIP:" + str(ifcfg[0]) + "\n"
    result += "\tNetmask:" + str(ifcfg[1]) + "\n"
    result += "\tGateway:" + str(ifcfg[2]) + "\n"
    result += "\tDNS:" + str(ifcfg[3]) + "\n"
    result +="\n"
    ifcfg = sta.ifconfig()
    result = "STA:\n"
    result += "\tIP:" + str(ifcfg[0]) + "\n"
    result += "\tNetmask:" + str(ifcfg[1]) + "\n"
    result += "\tGateway:" + str(ifcfg[2]) + "\n"
    result += "\tDNS:" + str(ifcfg[3]) + "\n"
    yield from socket.awrite(result)

#serves a static file from the filesystem
async def static(socket, file):
    yield from socket.awrite("HTTP/1.1 OK\r\n\r\n")
    f = open(file, 'rb')
    data = f.read(1024)
    while data:
        yield from socket.awrite(data)
        data = f.read(1024)
    f.close()

async def template(socket, file, env):
    yield from socket.awrite("HTTP/1.1 OK\r\n\r\n")
    t = open('base.html') #open base template (this one is usually longer)
    f = open(file) #open inserted template
    fcontent = f.read() #read inserted template completely into memory
    #"Handlebars"
    while "{{" in fcontent and "}}" in fcontent:
        gc.collect()
        expr = fcontent[fcontent.index("{{"):fcontent.index("}}")+2] #Get expression
        name = expr[2:-2]
        if name in env and env[name] != None:
            fcontent = fcontent.replace(expr, str(env[name])) #Replace expression
        else:
            fcontent = fcontent.replace(expr, "") #Delete expression
        yield
    #Iterate over the other file, replace fcontent if needed, write linewise
    for line in t:
        yield from socket.awrite(line.replace("{{~> page}}", fcontent))
    t.close()
    f.close()

async def redirect_to(writer, URL):
    yield from writer.awrite("HTTP/1.1 303 See Other\nLocation: " + URL + "\r\n\r\n\r\n\r\n")

async def handleWifiSettings(writer, env):
    if("AP_SSID" in  env):
        settings.set("AP_SSID", env["AP_SSID"])
    if("AP_PSK" in  env):
        settings.set("AP_PSK", env["AP_PSK"])
    if("STA_SSID" in  env):
        settings.set("STA_SSID", env["STA_SSID"])
    if("STA_PSK" in  env):
        settings.set("STA_PSK", env["STA_PSK"])
    settings.save()
    configureWifi()
    yield from redirect_to(writer, "/settings")

async def handleScaleCalibrate(writer, env):
    scale.set_factor(1)
    current_raw = scale.get_reading()
    set_weight = float(env["WEIGHT"])
    new_factor = set_weight / current_raw
    scale.set_factor(new_factor)
    settings.set("calibration_factor", new_factor)
    settings.save()
    yield from redirect_to(writer, "/overview")

async def handleServerSettings(writer, env):
    if "URL" in env:
        settings.set("Server-URL", env["URL"])
    if "Username" in env:
        settings.set("Server-Username", env["Username"])
    if "Password" in env:
        settings.set("Server-Password", env["Password"])
    settings.save()
    yield from redirect_to(writer, "/overview")

async def handleInventorySettings(writer, env):
    if "inventory_id" in env:
        settings.set("Inventory-Id", env["inventory_id"])
    settings.save()
    yield from redirect_to(writer, "/overview")
    
async def handleSettingsForm(writer):
    env = {}
    env["AP_SSID"] = settings.get("AP_SSID")
    env["AP_PSK"] = settings.get("AP_PSK")
    env["STA_SSID"] = settings.get("STA_SSID")
    env["STA_PSK"] = settings.get("STA_PSK")
    env["WEIGHT"] = scale.get_reading()
    env["URL"] = settings.get("Server-URL")
    env["Username"] = settings.get("Server-Username")
    env["Password"] = settings.get("Server-Password")
    env["INVENTORIES"] = "[]"
    if is_sta_connected():
        connection = Connection()
        if (yield from connection.check_connection()):
            env["INVENTORIES"] = await connection.get_inventories(string_instead_of_json=True)
            asyncio.get_event_loop().call_later_ms(100,connection.logout())
    yield from template(writer, "settings.html", env)

async def handleServerForm(writer):
    env = {}
    env["Server_URL"] = settings.get("Server-URL")
    env["Connection_OK"] = "No"
    env["Server_Weight"] = 0
    import utime as time
    env["Server_Time"] = time.time()
    env["Scale_Weight"] = int(scale.get_reading())
    connection = Connection()
    if (yield from connection.check_connection()):
        env["Connection_OK"] = "Yes"
        server_report = yield from connection.get_server_report()
        env["Server_Weight"] = server_report["weight_grams"]
        env["Server_Time"] = server_report["timestamp"]
    yield from template(writer, "server.html", env)

async def handleNetworkForm(writer):
    env = get_ifconfig_as_env()
    env["AP_SSID"] = settings.get("AP_SSID")
    env["STA_SSID"] = settings.get("STA_SSID")
    yield from template(writer, "network.html", env)

async def handleConfigForm(writer):
    fcontent = "<table>\n<thead>\n<tr><th>Setting</th><th>Value</th></tr>\n</thead>\n<tbody>\n"
    for key in settings.settings:
        fcontent += "<tr><td>" + str(key) + "</td><td>" + str(settings.get(key)) + "</td></tr>\n"
    fcontent += "</tbody>\n</table>"
    t = open("base.html")
    for line in t:
        yield from writer.awrite(line.replace("{{~> page}}", fcontent))
    t.close()

async def handleLogForm(writer):
    t = open("base.html")
    for line in t:
        if("{{~> page}}" in line):
            yield from writer.awrite("<pre style='width: 66vw'>")
            if "log.txt.old" in os.listdir("/"):
                f = open("log.txt.old", 'rb')
                data = f.read(1024)
                while data:
                    yield from writer.awrite(data)
                    data = f.read(1024)
                f.close()
            if "log.txt" in os.listdir("/"):
                f2 = open("log.txt", 'rb')
                data2 = f2.read(1024)
                while data2:
                    yield from writer.awrite(data2)
                    data2 = f2.read(1024)
                f2.close()
            else:
                yield from writer.awrite("LOG NOT FOUND")
            yield from writer.awrite("</pre>")
        else:
            yield from writer.awrite(line)
    t.close()
   

async def err(socket, code, message):
    yield from socket.awrite("HTTP/1.1 "+code+" "+message+"\r\n\r\n")
    yield from socket.awrite("<h1>"+message+"</h1>")

async def handle(reader, writer):
    try:
        l = yield from reader.readline()
        (method, url, version) = l.split(b" ")
        if b"?" in url:
            (path, query) = url.split(b"?", 2)
        else:
            (path, query) = (url, b"")
        logger().log("Request: " + str(method)[2:-1] + " " + str(path)[2:-1], "SERVER")
        while True:
            header = yield from reader.readline()
            if header == b"":
                return
            if header == b"\r\n":
                break

        if version != b"HTTP/1.0\r\n" and version != b"HTTP/1.1\r\n":
            yield from err(writer, "505", "Version Not Supported")
        elif method == b"GET":
            if path == b"/" or path == b"/overview":
                yield from template(writer, "overview.html", {})
            elif path == b"/settings":
                yield from handleSettingsForm(writer)
            elif path == b"/network":
                yield from handleNetworkForm(writer)
            elif path == b"/server":
               yield from handleServerForm(writer)
            elif path == b"/reading":
                yield from writeReading(writer)
            elif path == b"/weight":
                yield from writeSSEWeight(writer)
            elif path == b"/tare":
                yield from writeTare(writer)
            elif path ==b"/wifistatus":
                yield from writeWifiStatus(writer)
            elif path ==b"/config":
                yield from handleConfigForm(writer)
            elif path ==b"/log":
                yield from handleLogForm(writer)
            elif path.startswith(b"/static/"):
                yield from static(writer, path[1:])
            elif path == b"/redirect":
                async def handleRedirect(writer):
                    yield from redirect_to(writer, "/overview")
                yield from handleRedirect(writer)
            else:
                yield from err(writer, "404", "Not Found")
        elif method == b"POST":
            formdatastr = yield from reader.read()
            formdatastr = str(formdatastr)[2:-1]
            env = decode_formdata(formdatastr)
            if path == b"/wifi":
                yield from handleWifiSettings(writer, env)
            elif path == b"/scale":
                yield from handleScaleCalibrate(writer, env)
            elif path ==b"/server":
                yield from handleServerSettings(writer, env)
            elif path == b"/inventory":
                yield from handleInventorySettings(writer, env)
            else: 
                yield from err(writer, "404", "Not Found")
        else:
            yield from err(writer, "501", "Not Implemented")
        
        yield from writer.aclose()
    except OSError:
        logger().log("Socket closed", "SERVER")
    except BaseException as e:
        logger().log("Error 500: " + str(e), "SERVER")
        try:
            yield from err(writer, "500", "Internal Server Error\r\n\r\n" + str(e))
        except BaseException as e:
            logger().log("Error writing error response: " + str(e), "SERVER")


def start():
    asyncio.get_event_loop().call_soon(asyncio.start_server(handle, "192.168.4.1", 80))
    logger().log("Server running", "SERVER")