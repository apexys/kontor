var fs = require('fs');

class UserConfiguration{
    constructor(username){
        if(!fs.existsSync(__dirname + "/configs")){
            fs.mkdirSync(__dirname + "/configs");
        }
        this.path = __dirname + "/configs/" + encodeURIComponent(username);
        //Create config if it doesn't exist
        if(!fs.existsSync(this.path)){
            fs.writeFileSync(this.path, "{}");
        }
        this.load();
    }

    load(){
        this.config = JSON.parse(fs.readFileSync(this.path));
    }

    save(){
        fs.writeFileSync(this.path,JSON.stringify(this.config));
    }

    set(name, value){
        this.config[name] = value;
    }

    get(name){
        return this.config[name];
    }
}

module.exports = UserConfiguration;