import uasyncio as asyncio
import utime
import os

__logger = None

def logger():
    global __logger
    if __logger == None:
        __logger = Logger()
    return __logger


class Logger:
    async def set_time(self):
        from globals import sta_connected
        await sta_connected()
        await asyncio.sleep_ms(200)
        try:
            self.log("Trying to update time from NTP server", "LOGGER")
            import ntptime;
            ntptime.settime();
            self.log("Set time to " + str(utime.time()), "LOGGER")
        except BaseException as e:
            self.log("Error updating time: " + str(e), "LOGGER")
            self.log("Could not set time, all time measurements will be relative", "LOGGER")


    def __init__(self, maxloglines = 256):
        self.loglines = 0
        if "log.txt" in os.listdir("/"):
            l = open("log.txt");
            data = l.read(16)
            while data:
                if "\n" in str(data):
                    self.loglines += 1
                data = l.read(16)
            l.close()
        asyncio.get_event_loop().call_later(1, self.set_time())
        self.maxloglines = maxloglines

    def rebase_log(self):
        if "log.txt.old" in os.listdir("/"):
            os.remove("log.txt.old")
        os.rename("log.txt", "log.txt.old")

    def log(self, msg, scope = ""):
        if self.loglines > self.maxloglines:
            self.rebase_log()
        logmsg = ""
        if scope != "":
            logmsg = "[" + str(utime.time()) + "] " + scope + ": "  + msg
        else:
            logmsg = "[" + str(utime.time()) + "]: "  + msg
        logf = open("log.txt", "a")
        logf.write(logmsg + "\n")
        logf.close()
        self.maxloglines += 1
        print(logmsg)
        

