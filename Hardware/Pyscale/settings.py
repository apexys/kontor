from helpers import decode_formdata, encode_formdata
import os

__DEBUG = False

class Settings:
    def __init__(self):
        self.settings = {}
        self.load()

    def load(self):
        #Create settings file if it doesn't exist
        if not "settings.db" in os.listdir():
            open("settings.db", "a").close
        f = open("settings.db", "r")
        temp = decode_formdata(f.read())
        f.close()
        self.settings = {}
        if __DEBUG: print("Settings:")
        for key in temp:
            if __DEBUG: print("\t" + str(key) + " = " + str(eval(temp[key])))
            self.settings[key] = eval(temp[key])

    def save(self):
        temp = {}
        for key in self.settings:
            temp[key] = repr(self.settings[key])
        f = open("settings.db", "w")
        f.write(encode_formdata(temp))
        f.close()

    def get(self, key):
        if key in self.settings:
            return self.settings[key]
        else:
            return None

    def has(self, key):
        if key in self.settings:
            return True
        else:
            return False

    def set(self, key, value):
        self.settings[key] = value