/// # Reports
/// Reports are the basic measuring instrument for the inventory tracker.

/// ## Inventory Report
/// The Inventory Report type defines an Anchorpoint for ProductReports to one specific Inventory and Time
#[derive(SqlMacro, Serialize, Copy, Clone)]
pub struct InventoryReport{
    pub id: i64,
    pub timestamp: i64,
    pub inventory_id: i64,
    pub sensor_type: SensorType
}

impl InventoryReport{
    pub fn new(timestamp: i64, inventory_id: i64, sensor_type: SensorType) -> Self{
        InventoryReport{
            id: -1,
            timestamp,
            inventory_id,
            sensor_type
        }
    }
}

#[derive(Serialize, Copy, Clone)]
pub enum SensorType{
    Sensor,
    Manual
}

impl SensorType{
    pub fn from_str(s: &str) -> SensorType{
        match s {
            "Sensor" => SensorType::Sensor,
            "Manual" => SensorType::Manual,
            _ => unimplemented!()
        }
    }

    pub fn to_str(&self) -> &'static str{
        match self{
            SensorType::Sensor => "Sensor",
            SensorType::Manual => "Manual"
        }
    }

    pub fn from_string(s: String) -> SensorType{
        Self::from_str(&s)
    }
}

///The Product report type clarifies what amount of a certain product 
#[derive(SqlMacro, Serialize)]
pub struct ProductReport{
    pub id: i64,
    pub report_id: i64,
    pub product_id: i64,
    pub amount: i64
}

impl ProductReport{
    pub fn new(report_id: i64, product_id: i64, amount: i64) -> Self{
        ProductReport{
            id: -1,
            report_id,
            product_id,
            amount
        }
    }
}

#[derive(SqlMacro, Serialize)]
pub struct WeightReport{
    pub id: i64,
    pub timestamp: i64,
    pub inventory_id: i64,
    pub weight_grams: i64
}

impl WeightReport{
    pub fn new(timestamp: i64, inventory_id: i64, weight_grams: i64)->Self{
        WeightReport{
            id: -1,
            timestamp,
            inventory_id,
            weight_grams
        }
    }
}