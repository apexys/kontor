var request = require('request');

class Connection{

    constructor(serverURL){
        this.serverURL = serverURL;
        this.cookies = request.jar();
    }

    login(username, password){
        return new Promise((res,rej) => {
            request.post(this.serverURL+ '/login', {form:{username, password}, jar: this.cookies}) //Send post
                .on('error', (e) => rej(e)) //Pass error along
                .on('response', resp => {//We got a response, check if it's a valid one
                    if(this.cookies.getCookies(this.serverURL + "/login").map(c => c.key).includes('token')){//Valid response == "token" set in cookies
                    //Cookie docs: https://github.com/salesforce/tough-cookie
                        res(true);
                    }else{
                        res(false);
                    }
                });
        });
    }

    get_inventory_definitions(){
        return new Promise((res,rej) => {
            request.get(this.serverURL+ '/inventory_definitions_json', {jar: this.cookies}, function(error, response, body){
                if(error){
                    rej(error);
                }else{
                    res(JSON.parse(body))
                }
            });
        });
    }

    get_inventory_states(inventory_id, limit=10){
        return new Promise((res,rej) => {
            request.get(this.serverURL+ '/inventory_states_json/' + inventory_id + "/" + limit, {jar: this.cookies}, function(error, response, body){
                if(error){
                    rej(error);
                }else{
                    res(JSON.parse(body))
                }
            });
        });
    }

    logout(){
        return new Promise((res,rej) => {
            request.get(this.serverURL+ '/logout', {jar: this.cookies}) //Send post
                .on('error', (e) => rej(e)) //Pass error along
                .on('response', resp => {//We got a response, check if it's a valid one
                        res(true);
                });
        });
    }
}

module.exports = Connection;

