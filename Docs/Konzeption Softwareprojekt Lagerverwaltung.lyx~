#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\topmargin 2cm
\rightmargin 2cm
\bottommargin 2cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Konzeption Softwareprojekt Lagerverwaltung
\end_layout

\begin_layout Author
Valentin Buck
\end_layout

\begin_layout Date
24.07.2018
\end_layout

\begin_layout Section
Problemstellung
\end_layout

\begin_layout Standard
In einer Pflegeeinrichtung wird den Bewohnern 
\emph on
Wasser
\emph default
 in 
\emph on
Glasflaschen
\emph default
 bereitgestellt.
 Diese Wasserflaschen werden von einem 
\emph on
Anbieter
\emph default
 in 
\emph on
Transportkästen
\emph default
 aus Plastik geliefert und 
\emph on
stationsweise
\emph default
 in dedizierten 
\emph on
Lagerschränken
\emph default
 auf Vorrat gehalten und nach Bedarf von Pflegekräften 
\emph on
entnommen
\emph default
.
 Daraus ergeben sich drei Aufgabenstellungen, die von einer Software, die
 eine digitale Erfassung der Lagerbestände ermöglicht, gelöst werden sollen:
\end_layout

\begin_layout Enumerate
Der Wasserverbrauch pro Station ist abhängig von den Bewohnern der Station.
 Da besonders im Sommer Bewohner häufig zu wenig trinken soll die Software
 ermöglichen, Anomalien stationsweise festzustellen.
\end_layout

\begin_layout Enumerate
Um eine kontinuerliche Wasserversorgung sicherzustellen soll die Software
 vorhersagen können, wann ein gegebener Stationsvorrat aufgebraucht ist,
 sodass eine passende Bestellung aufgegeben werden kann.
\end_layout

\begin_layout Enumerate
Die Software sollte einen Anbieterwechsel und gegebenenfalls die Verwaltung
 weiterer Produkte einfach ermöglichen.
\end_layout

\begin_layout Section
Technologiestack und Datenmodell
\end_layout

\begin_layout Standard
Das Projekt besteht aus einer zentralen Serverkomponente, die eine Webanwendung
 als Benutzeroberfläche und eine Datenhaltung bereitstellt, sowie Gewichtssensor
en, die sich nach einer initialen Konfiguration mit der Serverkomponente
 verbinden.
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Vorläufig wird als Stack für die Serverkomponente folgende Konfiguration
 gewählt:
\end_layout

\begin_layout Itemize
Webserver Rocket.rs
\end_layout

\begin_layout Itemize
Datenhaltung in JSON-Dateien oder ggf.
 SQLite-Datenbanken
\end_layout

\begin_layout Itemize
Picnic.css und ECMAScript 6 für die Benutzeroberfläche
\end_layout

\begin_layout Standard
Das Datenmodell gliedert sich wie folgt:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
User
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Name
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Passwort
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Emailadresse
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Rolle (Admin, User, Sensor, Lieferant)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset space \qquad{}
\end_inset

 
\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Produkt
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Name
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Lieferant
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Nettogewicht
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Verpackungsgewicht
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset space \qquad{}
\end_inset

 
\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Verbund
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Bezeichnung
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Enthaltenes Produkt
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Anzahl enthaltener Produkte
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Leergewicht
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset

 
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Lagerplatz
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Name
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Gelagerte Produktart
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset space \qquad{}
\end_inset

 
\begin_inset Tabular
<lyxtabular version="3" rows="7" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Inventurbericht
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Datum
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Beschriebener Lagerplatz
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Typ (Sensor / Manuell)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Anzahl enthaltener Verbünde
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Anzahl enthaltener Produktverpackungen
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Anzahl enthaltener Produkte
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset

 
\begin_inset space \qquad{}
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Vorratsvorgabe
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Lagerplatz
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Anzahl zu lagernder Produkte
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="8" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Transaktionsbericht
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Datum
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Bearbeiter
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Lagerplatz
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Anzahl leere Verbünde entnommen
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Anzahl leere Verpackungen entnommen
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Anzahl Verbünde hinzugefügt
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Anzahl Produkte hinzugefügt
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Aus diesem Modell ergibt sich das folgende Benutzungsschema:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Ein 
\emph on
Admin-User
\emph default
 kann weitere User oder 
\emph on
Sensoren
\emph default
 anlegen und gegebenenfalls deren Attribute bearbeiten.
\begin_inset Newline newline
\end_inset

Ein solcher 
\emph on
User 
\emph default
kann dann Definitionen für 
\emph on
Produkte, Produktverbünde 
\emph default
(z.B.
 Glasflaschen mit Wasser und Wasserkästen) anlegen, sowie 
\emph on
Lagerplätze 
\emph default
definieren und für diese 
\emph on
Vorratsvorgaben
\emph default
 anlegen.
 Diese Vorgaben werden dann mithilfe von automatisierten oder manuellen
 
\emph on
Inventurberichten
\emph default
 überprüft.
 Wenn ein Lieferant einen Lagerplatz aufstockt legt er einen Transaktionsbericht
 an, um diese Aktion festzuhalten.
\end_layout

\begin_layout Section
Hardwarekomponente
\end_layout

\begin_layout Standard
Die Gewichtssensoren sind ein wichtiger Bestandteil des Projekts, da sie
 die Automatisierung der Inventurberichte möglich machen.
\begin_inset Newline newline
\end_inset

Die Sensoren sind als flache Plattformen gebaut, die Aufnahmemöglichkeiten
 für einen oder mehrere Projektverbünde bieten.
 
\begin_inset Newline newline
\end_inset

Zwischen der oberen und unteren Schicht der Plattform sind HX-711-kompatible
 Gewichtssensoren, sogenannte Wägezellen angebracht, die durch auf die Platform
 gestellte Produkte belastet werden.
 
\begin_inset Newline newline
\end_inset

Diese Sensoren werden durch einen Messumformer ausgelesen und der analoge
 Gewichtswert wird nach der Digitalisierung desselben an einen mit dem Internet
 verbundenen Mikroprozessor übermittelt.
\begin_inset Newline newline
\end_inset

Dieser Mikroprozessor kann sich als Sensor in die Serverkomponente einloggen
 und dort eine Definition des gelagerten Produkts sowie der relevanten Gewichtsw
erte von Produkt, Verpackung und Verbund abfragen.
\begin_inset Newline newline
\end_inset

Wird ein Produkt entnommen oder dazugestellt verändert sich der Gewichtswert
 auf der Plattform.
 
\begin_inset Newline newline
\end_inset

Der Mikroprozessor, der die Sensoren wiederholt abfragt, errechnet dann
 die Gewichtsdifferenz und korreliert diese mit den Gewichtswerten des relevante
n Produktverbunds.
\begin_inset Newline newline
\end_inset

Anhand dieser Korrelation und der Summe der für den Lagerplatz vorhandenen
 Inventurberichte erstellt der Mikroprozessor nun einen neuen Inventurbericht,
 der die aktuelle Veränderung miteinbezieht, kennzeichnet diesen als Sensorberic
ht und übermittelt diesen an die Serverkomponente.
\end_layout

\begin_layout Section
Definition des Funktionsumfangs via User-Stories
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-1 Beim ersten Start der Anwendung soll ein Admin-User erzeugt werden,
 sodass ein Nutzer sich als solcher einloggen kann
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-2 Ein Admin-User soll weitere Benutzer anlegen und bereits vorhandene
 Nutzer bearbeiten können, um weiteren Nutzern die Benutzung der Anwendung
 zu ermöglichen
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-3 Ein User soll in der Lage sein, sein Passwort und seine Emailadresse
 zu ändern, um den Admin-User zu entlasten
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-4 Ein User soll in der Lage sein, neue Produkte anzulegen und vorhandene
 Produkte zu bearbeiten, um neue Warenflüsse abbilden zu können
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-5 Ein User soll in der Lage sein, neue Produktverbünde anzulegen und
 vorhandene zu bearbeiten, sollten sich diese ändern
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-6 Ein User soll in der Lage sein, neue Lagerplätze anzulegen und die
 gelagerte Produktart festzulegen, um neue Stationen in das System einzubinden
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-7 Ein User soll in der Lage sein, die gelagerte Produktart in vorhandenen
 Lagerplätzen zu ändern, falls diese anders verwendet werden sollen
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-8 Ein User soll in der Lage sein, eine Vorratsvorgabe für einen Lagerplatz
 zu erstellen bzw.
 eine vorhandene Vorgabe zu ändern, um eine Überwachung der gelagerten Positione
n zu aktivieren
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-9 Ein User soll in der Lage sein, einen Inventurbericht zu erstellen,
 um dem System mitzuteilen, wie viele Einheiten eines Produkts auf einem
 gegebenen Lagerplatz vorhanden sind
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-10 Ein User soll die Möglichkeit haben, einzusehen wie viele Produkteinheiten
 auf allen Lagerplätzen vorhanden sind, sowie wann diese nach dem aktuellen
 Trend verbraucht sind um rechtzeitig eine Bestellung einleiten zu können
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-11 Ein User soll in der Lage sein, eine Bestellung für einen bestimmten
 Lieferanten vorzubereiten, indem alle einzukaufenden Produkte dieses Lieferande
n für alle Lagerplätze angezeigt werden, um nicht einzeln die Lagerplätze
 nach fehlenden Produkten durchgehen zu müssen
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-12 Ein User soll in der Lage sein, einen Sensor für einen bestimmten
 Lagerplatz einzurichten, sodass dieser automatisierte Inventurberichte
 liefern kann
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
US-13 Es sollte im Interface ersichtlich sein, ob eine Prognose von einem
 automatisierten oder einem manuellen Inventurbericht ausgeht, sodass auch
 bei fehlerhafter Hardware akkurate Prognosen möglich sind
\end_layout

\begin_layout Section
Vorraussichtliche Projektphasen
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
23.07
\begin_inset space ~
\end_inset

-
\begin_inset space ~
\end_inset

27.07 Konzeption der Software und Entwicklung eines ersten Prototypen
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
30.07
\begin_inset space ~
\end_inset

-
\begin_inset space ~
\end_inset

17.08 Verbesserung des Prototypen sowie Bestellung der Hardware
\end_layout

\begin_layout Labeling
\labelwidthstring 00.00.0000
20.08
\begin_inset space ~
\end_inset

-
\begin_inset space ~
\end_inset

03.09 Implementation der Sensorkomponente sowie abschließende Verbesserung
 des Prototypen
\end_layout

\end_body
\end_document
