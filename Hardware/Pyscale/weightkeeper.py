import uasyncio as asyncio
from uasyncio.synchro import Lock
from globals import scale, status
import utime as time
from logger import logger

stdev_threshold = 10 #Don't work with measurement blocks that have a standard deviation of higher than 10. Usually, any changes to the weight have stedevs of at least a thousand
weight_diff_threshold = 10 #The weight has to change at least 10g in order to warrant an update
loopsize = 10 #The loop measures every half a second. 10 Measurements is a loop every five.

transmitlock = Lock()


last_state = {}

DEBUG = False

async def weightkeeper_loop():
    logger().log("Standard Deviation Threshold: " + str(stdev_threshold), "WEIGHTKEEPER")
    logger().log("Weight Difference Threshold: " + str(weight_diff_threshold), "WEIGHKEEPER")
        
    if DEBUG: print("Waiting for zero")
    status.set(status.Code["transmitting"])    
    while not scale.tared:
        await asyncio.sleep_ms(100)
    status.set(status.Code["waiting"])
    logger().log("Starting capture", "WEIGHTKEEPER")

    loop = [0] * loopsize
    while True:
        for i in range(loopsize): #Take a reading every half a second
            tstart = time.ticks_ms() 
            loop[i] = scale.get_reading()
            while time.ticks_diff(time.ticks_ms(), tstart) < 500:
                await asyncio.sleep_ms(100)
        avg = 0
        for i in range(loopsize):
            avg += loop[i]
        avg = avg / loopsize
        stdev = 0
        for i in range(loopsize):
            stdev += (loop[i] - avg) ** 2
        
        if DEBUG:
            print(str(loopsize) + " measurements taken")
            print("\t Average: " + str(avg))
            print("\t Standard deviation: " + str(stdev))
        
        if stdev < stdev_threshold:
            if DEBUG: print("Valid measurement, stdev under threshold")

            if last_state == {}:
                if DEBUG: print("New data, no update")
                asyncio.get_event_loop().call_soon(report_weight_change(avg))
            else:
                weight_diff = abs(avg - last_state["avg"])
                if weight_diff > weight_diff_threshold:
                    if DEBUG: 
                        print("Measurement has considerable weight difference")
                        print("Weight difference: " + str(weight_diff))
                        print("New weight: " + str(avg))
                    status.set(status.Code["transmitting"])
                    asyncio.get_event_loop().call_soon(report_weight_change(avg))
                else:
                    if DEBUG: print("Measurement weight is pretty much the same (diff: "+ str(weight_diff) + ")" )

            #Finally, update the values
            last_state["avg"] = avg
            last_state["stdev"] = stdev
            if DEBUG: print("\n")

async def report_weight_change(weight):
    await transmitlock.acquire() # Transmit in order
    logger().log("Starting transmission of new weight of " + str(weight), "WEIGHTKEEPER")
    if weight < 0: #Negative weights tend to confuse the server
        weight = 0
    status.set("transmitting")
    from connection import Connection
    connection = Connection()
    if not await connection.check_connection():
        logger().log("Server not connected, update not possible", "WEIGHTKEEPER")
    else:
        server_weight = int((await connection.get_server_report())["weight_grams"])
        if abs(server_weight - weight) > weight_diff_threshold:
            if not await connection.report_weight(weight):
                logger().log("Error reporting weight")
            else:
                logger().log("Weight of " + str(weight) + " reported to server", "WEIGHTKEEPER")
        else:
            logger().log("Weight is too similar to the one stored on the server. Not transmitting.", "WEIGHTKEEPER")
        await connection.logout()
    status.set("ok")
    transmitlock.release()
